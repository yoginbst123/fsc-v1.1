$(document).ready(function(){
    $('#new-hiring-table').dataTable({
        "aoColumnDefs": [
            {"bSortable": false, "aTargets": [0, 1, 2, 3, 4]}
        ]
    });
    $('#new-application-table').dataTable({
        "aoColumnDefs": [
            {"bSortable": false, "aTargets": [0, 1, 2, 3, 4]}
        ]
    });
    $('#new-candidate-table').dataTable({
        "aoColumnDefs": [
            {"bSortable": false, "aTargets": [0, 1, 2, 3, 4]}
        ]
    });
});
