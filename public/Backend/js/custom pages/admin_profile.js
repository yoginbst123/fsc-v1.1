$(document).ready(function () {
    /*
    all javascript logic of admin profile page.
    first describe logic parts and another part for validation and ajax calling.
     */


    /* Custome Function */

    const libs = {
        // realtime Typing function pass value (object,value)

        realTimeTyping: function (input, output) {
            const isSelected = $('#checkmeout0').is(':checked');
            if (isSelected) {
                output.val(input.val());
            }
        },
        setJurisdication: function (boolval) {
            if (boolval == "City") {
                $('.show_license_city').removeClass('d-none');
                $('#license_county_div').addClass('d-none');
            } else if (boolval == "County") {
                $('.show_license_city').addClass('d-none');
                $('#license_county_div').removeClass('d-none');
            }
        },
        isSamePhysicalAddress: function (boolval) {
            const isSelected = $(boolval).is(':checked');
            const reg_address1 = $('#reg_address_1').val();
            const reg_address2 = $('#reg_address_2').val();
            const reg_city = $('#reg_city').val();
            const reg_state = $('#state').val();
            const reg_zipcode = $('#reg_zipcode').val();
            if (isSelected) {
                $('.hasReadonly').attr('readonly', 'readonly');
                $('#pa_address_1').val(reg_address1);
                $('#pa_address_2').val(reg_address2);
                $('#pa_city').val(reg_city);
                $('#pa_state').val(reg_state);
                $('#pa_state_input').val(reg_state);
                $('#pa_zip').val(reg_zipcode);
                $('#inputTag').removeClass('d-none');
                $('#pa_state_input').attr('name', 'pa_state');
                $('#pa_state').attr('name', 'pa_state__');
                $('#selectTag').addClass('d-none');
            } else {
                $('.hasReadonly').removeAttr('readonly');
                $('#pa_address_1').val('');
                $('#pa_address_2').val('');
                $('#pa_city').val('');
                $('#pa_state').val('');
                $('#pa_state_code').val('');
                $('#pa_zip').val('');
                $('#inputTag').addClass('d-none');
                $('#selectTag').removeClass('d-none');
                $('#pa_state').attr('name', 'pa_state');
                $('#pa_state_input').attr('name', 'pa_state__');
            }
        },
        isSameAsFax: function (boolval) {
            const isSelected = $(boolval).is(':checked');
            const fax_no = $('#fax_no').val();
            if (isSelected) {
                $('.hasReadonly').attr('readonly', 'readonly');
                $('#contact_fax_no').val(fax_no);
            } else {
                $('.hasReadonly').removeAttr('readonly');
                $('#contact_fax_no').val('');
            }
        },
        setQuestionValue: function (input, output) {
            const selectedVal = $(input).find(':selected').text();
            $(output).val(selectedVal);
        },
        responsess: function (result) {
            console.log(result);
        },
        ajaxCalling: function (url, data, method,callback) {
            $.ajax({
                url: url,
                method: method,
                data: data,
                success: callback,
                error: function (error) {
                    //returnval = error;
                }
            });
        }

    }

    const jurisdiction_value = $('#jurisdiction').find('selected').val();
    libs.setJurisdication(jurisdiction_value);
    $('#jurisdiction').change(function () {
        libs.setJurisdication($(this).val());
    });

    /* All Fields Masking */
    $('.js-select-2').select2();

    // $("#ext1").inputFilter(function(value) {
    //     return /^\d*$/.test(value);    // Allow digits only, using a RegExp
    // });

    /* In Company Info Profile */
    libs.isSamePhysicalAddress($('#checkmeout0'));
    $('#checkmeout0').click(function () {
        libs.isSamePhysicalAddress(this);
    });


    $('#reg_address_1').keyup(function () {
        var output = $('#pa_address_1');
        libs.realTimeTyping($(this), output);
    });
    $('#reg_address_2').keyup(function () {
        var output = $('#pa_address_2');
        libs.realTimeTyping($(this), output);
    });
    $('#reg_city').keyup(function () {
        var output = $('#pa_city');
        libs.realTimeTyping($(this), output);
    });
    $('#reg_zipcode').keyup(function () {
        var output = $('#pa_zip');
        libs.realTimeTyping($(this), output);
    });
    $('#reg_state').keyup(function () {
        var output = $('#pa_state');
        libs.realTimeTyping($(this), output);
    });

    /* In Contact Info Profile */
    libs.isSameAsFax($('#checkmeout1'));
    $('#checkmeout1').click(function () {
        libs.isSameAsFax(this);
    });

    /* In Security Info Profile */
    $('#que_1').change(function () {
        libs.setQuestionValue($(this), $('#question1'));
    });
    $('#que_2').change(function () {
        libs.setQuestionValue($(this), $('#question2'));
    });
    $('#que_3').change(function () {
        libs.setQuestionValue($(this), $('#question3'));
    });

    /* In Profile Formation Tab */
    var getSelectedOfCorporation = $('#coporation').is(':selected');
    var getSelectedOfLLC = $('#llc').is(':selected');
    if (getSelectedOfCorporation) {
        $('#certificateText').text('Certification Of Corporation');
        $('#certificateLabel').text('Certification Of Corporation');
        $('#articleText').text('Article Of Corporation');
        $('#articlelabel').text('Article Of Corporation');
    }
    if (getSelectedOfLLC) {
        $('#certificateText').text('Certification Of LLC');
        $('#certificateLabel').text('Certification Of LLC');
        $('#articleText').text('Article Of LLC');
        $('#articlelabel').text('Article Of LLC');
    }
    $('#coporation').click(function () {
        $('#certificateText').text('Certification Of Corporation');
        $('#certificateLabel').text('Certification Of Corporation');
        $('#articleText').text('Article Of Corporation');
        $('#articlelabel').text('Article Of Corporation');
    });
    $('#llc').click(function () {
        $('#certificateText').text('Certification Of LLC');
        $('#certificateLabel').text('Certification Of LLC');
        $('#articleText').text('Article Of LLC');
        $('#articlelabel').text('Article Of LLC');
    });

    $('#certificateLabel').click(function () {
        const val = $(this).attr('data-src');
        alert(val);
    });

    /*
        Form Validation Area
     */
    $("#storeProfile").validate({
        rules: {
            company_name: {required: true},
            business_name: {required: true},
            address_1: {required: true},
            city: {required: true},
            state: {required: true},
            zip: {required: true},
            pa_city: {required: true},
            pa_state: {required: true},
            pa_zip: {required: true},
            pa_address_1: {requird: true},
            contact_no: {required: true}
        },
        messages: {
            company_name: {required: "Company name must required !"},
            business_name: {required: "Business name must required !"},
            address_1: {required: "Address must required !"},
            city: {required: "City must required !"},
            state: {required: "state must required !"},
            zip: {required: "Zip must required !"},
            pa_city: {required: "Physical City must required !"},
            pa_state: {required: "Physical State must required !"},
            pa_zip: {required: "Physical Zip must required !"},
            pa_address_1: {required: "Physical Address must required !"},
            contact_no: {required: "Contact must required !"}
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            $(e).closest('.form-tab').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).closest('.form-tab').removeClass('has-error'); //.addClass('has-info');
            $(e).remove();
        },
        submitHandler: function (form) {
            form.submit();

        }
    });

    $("#storeContact").validate({
        rules: {
            fname: {required: true},
            mname: {required: true},
            lname: {required: true},
            city: {required: true},
            state: {required: true},
            zip: {required: true},
            contact_no_1: {required: true}
        },
        messages: {
            fname: {required: "First name must be required !"},
            mname: {required: "Middle name must be required !"},
            lname: {required: "Last Name must be required !"},
            city: {required: "City must required !"},
            state: {required: "state must required !"},
            zip: {required: "Zip must required !"},
            contact_no_1: {required: "Contact must required !"}
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            $(e).closest('.form-tab').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).closest('.form-tab').removeClass('has-error'); //.addClass('has-info');
            $(e).remove();
        },
        submitHandler: function (form) {
            form.submit();

        }
    });


    /*
    Ajax Calling Function For Formation
     */
    $('#certificateDelete').click(function () {
        const url = $(this).attr('data-src');
        const token = $(this).attr('data-token');
        const method = "POST";
        const data = {
            '_token': token,
            'certificate': 'certificate',
            'id': 1
        };
        libs.ajaxCalling(url, data, method,function(response){
            console.log(response);
        });

    });
    $('#articlesDelete').click(function () {
        const url = $(this).attr('data-src');
        const token = $(this).attr('data-token');
        const method = "POST";
        const data = {
            '_token': token,
            'certificate': 'article',
            'id': 1
        };
        libs.ajaxCalling(url, data, method,function(response){
            console.log(response);
        });

    });

    $('.addShareHolderEvent').on("click",function (event) {
        var formdata = $('#storeShareholder').serialize();
        const url = $(this).attr('data-src');
        const data = formdata;
        const method = "POST";
        libs.ajaxCalling(url, data, method,function(response){
            var jsonData = JSON.parse(response);
            $('#Share-Holder-table tbody').empty();
            for(var i=0;i<jsonData.length;i++){
                var no = i+1;
                let sharholderData = "<tr>";
                sharholderData += "<td>"+ no +"</td>";
                sharholderData += "<td>" + jsonData[i]['fname'] +" "+jsonData[i]['mname']+" "+jsonData[i]['lname']+"</td>";
                sharholderData += "<td>"+jsonData[i]['position']+"</td>";
                sharholderData += "<td>"+jsonData[i]['percentage']+"%</td>";
                sharholderData += "<td>"+jsonData[i]['effective_date']+"</td>";
                if(jsonData[i]['status']=='active'){
                    sharholderData += "<td><span class='badge badge-status badge-outline-success badge-pill'>Active</span></td>";
                }else if(jsonData[i]['status']=='inactive'){
                    sharholderData += "<td><span class='badge badge-status badge-outline-danger badge-pill'>In-Active</span></td>";
                }
                sharholderData += '<td>' +
                    '<a href=""><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>' +
                    '</td>';
                $('#Share-Holder-table tbody').append(sharholderData);
            }
            $('#modal-Share-holder-records').modal('hide');
        });

    });

    /*
        Dynamic Models Formation
     */
    $('.dynamic-models').click(function () {
        const dataURL = $(this).attr('data-href');
        $('.modal-body').load(dataURL, function () {
            $('#modal-Share-holder-records').modal({show: true});
        });
    });

});
function responseofAjax(response){
    var val = response;
    return val;
}
