<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin/models/share-account', function () {
    return view('Backend.Admin.Models.share-account');
})->name('models.share-holder');

//Admin Calling View With Data
Route::get('/admin/profile',[\App\Http\Controllers\Admin\ProfileController::class,'index'])->name('admin.profile');
Route::get('/admin/work-status',[\App\Http\Controllers\Admin\WorkStatusController::class,'index'])->name('admin.work-status');
Route::get('/admin/uploads',[\App\Http\Controllers\Admin\UploadsController::class,'index'])->name('admin.uploads');
Route::get('/admin/editEmployee',[\App\Http\Controllers\Employee\EmployeeController::class,'edit'])->name('admin.editEmployee');

//Admin Actions
Route::post('/admin/storeProfile',[\App\Http\Controllers\Admin\ProfileController::class,'storeCompanyInfo'])->name('admin.storeProfile');
Route::post('/admin/storeContact',[\App\Http\Controllers\Admin\ProfileController::class,'storeContact'])->name('admin.storeContact');
Route::post('/admin/storeSecurity',[\App\Http\Controllers\Admin\ProfileController::class,'storeSecurity'])->name('admin.storeSecurity');
Route::post('/admin/storeFormation',[\App\Http\Controllers\Admin\ProfileController::class,'storeFormation'])->name('admin.storeFormation');
Route::post('/admin/storeShareHolder',[\App\Http\Controllers\Admin\ProfileController::class,'storeShareHolder'])->name('admin.storeShareHolder');

/*
 * Admin Work status Routes
 * start
 */
Route::post('/admin/storeCorporationLicense',[\App\Http\Controllers\Admin\WorkStatusController::class,'storeCorporationLicense'])->name('admin.storeCorporationLicense');
Route::post('/admin/storeShareLedger',[\App\Http\Controllers\Admin\WorkStatusController::class,'storeShareLedger'])->name('admin.storeShareLedger');
Route::post('/admin/storeBusinessLicense',[\App\Http\Controllers\Admin\WorkStatusController::class,'storeBusinessLicense'])->name('admin.storeBusinessLicense');


/*
 * Ajax Calling Routes
 * start
 */

Route::post('/admin/removeCertificate',[\App\Http\Controllers\Admin\ProfileController::class,'removeCertificate'])->name('admin.removeCertificate');

/*
 * For Coporation
 */
Route::post('/admin/editCorporation',[\App\Http\Controllers\Admin\WorkStatusController::class,'editCorporation'])->name('admin.editCorporation');


/*
 * End
 */

Route::get('/Employee/hiring',[\App\Http\Controllers\employee\HiringController::class,'index'])->name('employee.hiring');
Route::get('/Employee/application',[\App\Http\Controllers\employee\ApplicationController::class,'index'])->name('employee.application');
Route::get('/Employee/candidate',[\App\Http\Controllers\employee\CandidateController::class,'index'])->name('employee.candidate');
Route::get('/Employee/supervisor',[\App\Http\Controllers\employee\EmployeeController::class,'supervisorList'])->name('employee.supervisor');
Route::get('/Employee/employees',[\App\Http\Controllers\employee\EmployeeController::class,'index'])->name('employee.employees');
Route::get('/Employee/create',[\App\Http\Controllers\employee\EmployeeController::class,'create'])->name('employee.create');

/*
 * Setup menu Routes
 */

Route::get('/setup/bank-list',[\App\Http\Controllers\Setup\BankController::class,'index'])->name('setup.bank-list');
Route::get('/setup/bank-add',[\App\Http\Controllers\Setup\BankController::class,'create'])->name('setup.bank-add');

Route::get('/FSC/client-registration',[\App\Http\Controllers\employee\EmployeeController::class,'create'])->name('fsc.client');

Route::group(['prefix'=>'admin','middleware'=>'admin:admin'],function (){
    Route::get('/login',[\App\Http\Controllers\AdminController::class,'loginForm']);
    Route::post('/login',[\App\Http\Controllers\AdminController::class,'store'])->name('admin.login');
    /*
     * Setup Admin Profiles page Routes
     */
});

Route::middleware(['auth:sanctum,admin', 'verified'])->get('/admin/dashboard', function () {
    return view('Backend.dashboard');
})->name('admin.dashboard');

Route::middleware(['auth:sanctum,web', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
