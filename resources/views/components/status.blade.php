@if($status == "active" || $status == "Active")
    <span class="badge badge-status badge-outline-success badge-pill">{{$status}}</span>
@elseif ($status == "inactive" || $status == "Inactive")
    <span class="badge badge-status badge-outline-warning badge-pill">{{$status}}</span>
@elseif ($status=="hold" || $status=="Hold")
    <span class="badge badge-status badge-outline-danger badge-pill">{{$status}}</span>
@elseif ($status=="wait" || $status=="Wait")
    <span class="badge badge-status badge-outline-primary badge-pill">{{$status}}</span>

@endif
