@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Admin Profile</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Admin/Profile</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-4">Admin Profile</h4>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="nav flex-column nav-pills nav-pills-tab" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    <a class="nav-link active show mb-1" id="v-pills-company-tab" data-toggle="pill" href="#v-pills-company" role="tab" aria-controls="v-pills-home"
                                       aria-selected="true">
                                        Company Info</a>
                                    <a class="nav-link mb-1" id="v-pills-contact-tab" data-toggle="pill" href="#v-pills-contact" role="tab" aria-controls="v-pills-profile"
                                       aria-selected="false">
                                        Contact Info</a>
                                    <a class="nav-link mb-1" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-messages"
                                       aria-selected="false">
                                        Security Info</a>
                                    <a class="nav-link mb-1" id="v-pills-formation-tab" data-toggle="pill" href="#v-pills-formation" role="tab" aria-controls="v-pills-settings"
                                       aria-selected="false">
                                        Formation</a>
                                    <a class="nav-link mb-1" id="v-pills-license-tab" data-toggle="pill" href="#v-pills-license" role="tab" aria-controls="v-pills-home"
                                       aria-selected="true">
                                        License</a>
                                    <a class="nav-link mb-1" id="v-pills-taxation-tab" data-toggle="pill" href="#v-pills-taxation" role="tab" aria-controls="v-pills-profile"
                                       aria-selected="false">
                                        Taxation</a>
                                    <a class="nav-link mb-1" id="v-pills-banking-tab" data-toggle="pill" href="#v-pills-banking" role="tab" aria-controls="v-pills-messages"
                                       aria-selected="false">
                                        Banking</a>
                                    <a class="nav-link mb-1" id="v-pills-other-tab" data-toggle="pill" href="#v-pills-other" role="tab" aria-controls="v-pills-settings"
                                       aria-selected="false">
                                        Others</a>
                                </div>
                            </div> <!-- end col-->


                            <div class="col-sm-8">
                                <div class="tab-content pt-0">
                                    <div class="tab-pane fade active show" id="v-pills-company" role="tabpanel" aria-labelledby="v-pills-company-tab">
                                        <form action="{{route('admin.storeProfile')}}" method="post" name="storeProfile" id="storeProfile">
                                            @csrf
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    @foreach ($errors->all() as $error)
                                                        <div><i class="icon-warning-sign"></i> {{ $error }}</div>
                                                    @endforeach
                                                </div>
                                            @endif
                                            <input type="hidden" value="{{\Illuminate\Support\Facades\Auth::user()->id}}" name="admin_id">
                                            <input type="hidden" @if(isset($isProfileExsist)) @if($isProfileExsist > 0) name=profile_id @endif @endif value="1">
                                            <h4 class="header-title text-center bg-icon-warning mb-1">Company Information</h4>
                                            <hr/>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="company_name_lbl">Company Name <span>(Legal Name)</span> <span class="text-danger">*</span></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="company_name_lbl" value="@if($isProfileExsist > 0) {{$companyInfo->company_name}} @else {{old('company_name')}} @endif" name="company_name">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="business_name"> Business Name <span class="text-danger">*</span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="business_name" value="@if($isProfileExsist > 0) {{$companyInfo->business_name}} @else {{old('business_name')}} @endif" name="business_name" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_address_1">Address1 <span class="text-danger">*</span></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="@if($isProfileExsist > 0) {{$companyInfo->address_1}} @else {{old('address_1')}} @endif" id="reg_address_1" name="address_1">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_address_2">Address2</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="@if($isProfileExsist > 0) {{$companyInfo->address_2}} @else {{old('address_2')}} @endif" id="reg_address_2" name="address_2">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_city">City / State / Zip <span class="text-danger">*</span></label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" value="@if($isProfileExsist > 0) {{$companyInfo->city}} @else {{old('city')}} @endif" id="reg_city" name="city">
                                                </div>
                                                <div class="col-md-3">
                                                    <x-state data="state" message="{{$companyInfo->state}}"/>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control input-usa-zip-mask" value="@if($isProfileExsist > 0){{$companyInfo->zip}}@else{{old('zip')}}@endif" id="reg_zipcode" name="zip">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-form-label col-md-3 text-right">Telephone #. <span class="text-danger">*</span></label>
                                                <div class="col-md-3">
                                                    <input name="contact_no" type="text" value="@if($isProfileExsist > 0){{$companyInfo->contact_no}}@else{{old('contact_no')}}@endif" id="contact_no" class="form-control input-tel-mask" placeholder="(999) 999-9999"/>
                                                </div>
                                                <div class="col-md-3">
                                                    <select name="contact_no_type" id="contact_no_type" class="form-control">
                                                        <option value="Office">Office</option>
                                                        <option value="Mobile">Mobile</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input class="form-control input-extension-mask" id="ext1" maxlength="5" name="extension" value="@if($isProfileExsist > 0){{$companyInfo->extension}}@else{{old('extension')}}@endif" placeholder="Ext" type="text">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="fax_no">Fax #.</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="@if($isProfileExsist > 0){{$companyInfo->fax_no}}@else{{old('fax_no')}}@endif" id="fax_no" name="fax_no">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="email">Email</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="@if($isProfileExsist > 0){{$companyInfo->email}}@else{{old('email')}}@endif" id="email" name="email">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="website">Website</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="@if($isProfileExsist > 0){{$companyInfo->website}}@else{{old('website')}}@endif" id="website" name="website">
                                                </div>
                                            </div>
                                            <hr/>
                                            <h4 class="header-title text-center bg-icon-warning mb-1">Physical Address</h4>
                                            <hr/>

                                            <div class="form-group mb-3">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="same_as_company_info" value="@if($isProfileExsist > 0) 1 @endif" @if($isProfileExsist > 0) checked @endif class="custom-control-input" id="checkmeout0">
                                                    <label class="custom-control-label" for="checkmeout0">Same as Company Address</label>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="pa_address_1">Physical Address1</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control hasReadonly" id="pa_address_1" value="@if($isProfileExsist > 0) {{$companyInfo->pa_address_1}} @else {{old('pa_address_1')}} @endif" name="pa_address_1">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="pa_address_2">Physical Address2</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control hasReadonly" id="pa_address_2" value="@if($isProfileExsist > 0) {{$companyInfo->pa_address_2}} @else {{old('pa_address_2')}} @endif" name="pa_address_2">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="pa_city">City / State / Zip</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control hasReadonly" value="@if($isProfileExsist > 0) {{$companyInfo->pa_city}} @else {{old('pa_city')}} @endif" id="pa_city" name="pa_city">
                                                </div>
                                                <div class="col-md-3 d-none" id="inputTag">
                                                    <input type="text" class="form-control hasReadonly" value="@if($isProfileExsist > 0) {{$companyInfo->pa_state}} @else {{old('pa_state')}} @endif" id="pa_state_input" name="pa_state">
                                                </div>
                                                <div class="col-md-3" id="selectTag">
                                                    <x-state data="pa_state" message="{{$companyInfo->pa_state}}"/>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control hasReadonly input-usa-zip-mask" value="@if($isProfileExsist > 0) {{$companyInfo->pa_zip}} @else {{old('pa_zip')}} @endif" id="pa_zip" name="pa_zip">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="county">County / Code</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" value="@if($isProfileExsist > 0) {{$companyInfo->county}} @else {{old('county')}} @endif" id="county" name="county">
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" value="@if($isProfileExsist > 0) {{$companyInfo->county_code}} @else {{old('county_code')}} @endif" id="county_code" name="county_code" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3 text-center">
                                                <div class="col-md-6">
                                                    @if(isset($isProfileExsist))
                                                        @if($isProfileExsist > 0)
                                                            <button type="submit" class="col-md-5 btn btn-primary text-center">Update</button>
                                                        @else
                                                            <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                                        @endif
                                                    @endif
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="reset" class=" col-md-5 btn btn-danger text-center">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-contact" role="tabpanel" aria-labelledby="v-pills-contact-tab">
                                        <form action="{{route('admin.storeContact')}}" name="storeContact" id="storeContact" method="post">
                                            @csrf
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    @foreach ($errors->all() as $error)
                                                        <div><i class="icon-warning-sign"></i> {{ $error }}</div>
                                                    @endforeach
                                                </div>
                                            @endif
                                            <input type="hidden" value="{{\Illuminate\Support\Facades\Auth::user()->id}}" name="admin_id">
                                            <input type="hidden" @if(isset($isContactExist)) @if($isContactExist > 0) name=contact_id value="{{$contactInfo->id}} @endif @endif ">
                                            <h4 class="header-title text-center bg-icon-warning mb-1">Contact Information</h4>
                                            <hr/>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="name">Name</label>
                                                <div class="col-md-2">
                                                    <select name="prefix_name" id="prefix_name" class="form-control">
                                                        <option value="Mr">Mr.</option>
                                                        <option value="Mrs">Mrs.</option>
                                                        <option value="Miss">Miss.</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="name" name="fname" value="@if($isContactExist > 0) {{$contactInfo->fname}} @else {{old('fname')}} @endif">
                                                </div>
                                                <div class="col-md-1">
                                                    <input type="text" class="form-control" id="name" name="mname" value="@if($isContactExist > 0) {{$contactInfo->mname}} @else {{old('mname')}} @endif">
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="name" name="lname" value="@if($isContactExist > 0) {{$contactInfo->lname}} @else {{old('lname')}} @endif">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_address">Address1</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="reg_address" name="address_1" value="@if($isContactExist > 0) {{$contactInfo->address_1}} @else {{old('address_1')}} @endif">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_address">Address2</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="reg_address" name="address_2" value="@if($isContactExist > 0) {{$contactInfo->address_2}} @else {{old('address_2')}} @endif">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_city">City / State / Zip</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="reg_city" name="city" value="@if($isContactExist > 0) {{$contactInfo->state}} @else {{old('city')}} @endif">
                                                </div>
                                                <div class="col-md-3">
                                                    @if($isContactExist > 0)
                                                        <x-state data="state" message="{{$contactInfo->state}}"/>
                                                    @else
                                                        <x-state data="state" message="GA"/>
                                                    @endif
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control input-usa-zip-mask" id="reg_city" name="zip" value="@if($isContactExist > 0){{$contactInfo->zip}}@else{{old('zip')}}@endif">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-form-label col-md-3 text-right">Telephone #. <span class="text-danger">*</span></label>
                                                <div class="col-md-3">
                                                    <input name="contact_no_1" value="@if($isContactExist > 0){{$contactInfo->contact_no_1}}@else{{old('contact_no_1')}}@endif" type="tel" id="contact_no_1" class="form-control input-tel-mask" placeholder="(999) 999-9999"/>
                                                </div>
                                                <div class="col-md-3">
                                                    <select name="contact_no_type_1" id="contact_no_type_1" class="form-control">
                                                        <option value="Office">Office</option>
                                                        <option value="Mobile">Mobile</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input class="form-control input-extension-mask" id="contact_no_extension_1" maxlength="5" name="contact_no_extension_1" value="@if($isContactExist > 0){{$contactInfo->contact_no_extension_1}}@else {{old('contact_no_extension_1')}} @endif" placeholder="Ext" type="text">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-form-label col-md-3 text-right">Telephone # 2. <span class="text-danger">*</span></label>
                                                <div class="col-md-3">
                                                    <input name="contact_no_2" value="@if($isContactExist > 0){{$contactInfo->contact_no_2}}@else{{old('contact_no_2')}}@endif" type="tel" id="contact_no_2" class="form-control input-tel-mask" placeholder="(999) 999-9999"/>
                                                </div>
                                                <div class="col-md-3">
                                                    <select name="contact_no_type_2" id="contact_no_type_2" class="form-control">
                                                        <option value="Office">Office</option>
                                                        <option value="Mobile">Mobile</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input class="form-control input-extension-mask" id="contact_no_extension_2" maxlength="5" name="contact_no_extension_2" value="@if($isContactExist > 0){{$contactInfo->contact_no_extension_2}}@else {{old('contact_no_extension_2')}} @endif" placeholder="Ext" type="text">
                                                </div>
                                            </div>

                                            <div class="form-group mb-3">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="same_as_company_fax_no" value="1" class="custom-control-input" id="checkmeout1">
                                                    <label class="custom-control-label" for="checkmeout1">Same as Company Fax No</label>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="fax_no">Fax #.</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control input-fax-no-mask hasReadonly" id="contact_fax_no" name="fax_no" value="@if($isContactExist > 0){{$contactInfo->fax_no}}@else{{old('fax_no')}}@endif">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="email">Email</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="email" name="email" value="@if($isProfileExsist > 0){{$companyInfo->email}}@else{{old('email')}}@endif">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3 text-center">
                                                <div class="col-md-6">
                                                    @if(isset($isContactExist))
                                                        @if($isContactExist > 0)
                                                            <button type="submit" class="col-md-5 btn btn-primary text-center">Update</button>
                                                        @else
                                                            <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                                        @endif
                                                    @else
                                                        <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                                    @endif
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="reset" class=" col-md-5 btn btn-danger text-center">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                        <form action="{{route('admin.storeSecurity')}}" method="post">
                                            @csrf
                                            <input type="hidden" name="admin_id" value="{{Auth::user()->id}}">
                                            <input type="hidden" @if($isSecurityExist>0) name="security_id" value="{{$securityInfo->id}}" @endif>
                                            <h4 class="header-title text-center bg-icon-warning mb-1">Security Information</h4>
                                            <hr/>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="email">Username</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="{{Auth::user()->email}}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="fax_no">Security Q.1</label>
                                                <div class="col-md-9">
                                                    <select name="que_1" id="que_1" class="form-control">
                                                        <option value="">---Select---</option>
                                                        <option value="1" @if($isSecurityExist>0) {{$securityInfo->que_1 == 1 ? 'selected' : ''}} @endif>What was your favorite place to visit as a child?</option>
                                                        <option value="2" @if($isSecurityExist>0) {{$securityInfo->que_1 == 2 ? 'selected' : ''}} @endif>Who is your favorite actor, musician, or artist?</option>
                                                        <option value="3" @if($isSecurityExist>0) {{$securityInfo->que_1 == 3 ? 'selected' : ''}} @endif>What is the name of your favorite pet?</option>
                                                        <option value="4" @if($isSecurityExist>0) {{$securityInfo->que_1 == 4 ? 'selected' : ''}} @endif>In what city were you born?</option>
                                                        <option value="5" @if($isSecurityExist>0) {{$securityInfo->que_1 == 5 ? 'selected' : ''}} @endif>What is the name of your first school?</option>
                                                    </select>
                                                    <input type="hidden" id="question1" name="question1" value="@if($isSecurityExist>0){{$securityInfo->question1}}@endif">
                                                </div>

                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="answer1">Answer 1</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="answer1" name="answer1" required value="@if($isSecurityExist>0){{$securityInfo->answer1}}@endif">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="que_2">Security Q.2</label>
                                                <div class="col-md-9">
                                                    <select name="que_2" id="que_2" class="form-control">
                                                        <option value="">---Select---</option>
                                                        <option value="1" @if($isSecurityExist>0) {{$securityInfo->que_2 == 1 ? 'selected' : ''}} @endif>What is your favorite movie?</option>
                                                        <option value="2" @if($isSecurityExist>0) {{$securityInfo->que_2 == 2 ? 'selected' : ''}} @endif>What was the make of your first car?</option>
                                                        <option value="3" @if($isSecurityExist>0) {{$securityInfo->que_2 == 3 ? 'selected' : ''}} @endif>What is your favorite color?</option>
                                                        <option value="4" @if($isSecurityExist>0) {{$securityInfo->que_2 == 4 ? 'selected' : ''}} @endif>What is your father's middle name?</option>
                                                        <option value="5" @if($isSecurityExist>0) {{$securityInfo->que_2 == 5 ? 'selected' : ''}} @endif>What is the name of your first grade teacher?</option>
                                                    </select>
                                                    <input type="hidden" id="question2" name="question2" value="@if($isSecurityExist>0){{$securityInfo->question2}}@endif">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="answer2">Answer 2</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="answer2" name="answer2" value="@if($isSecurityExist>0){{$securityInfo->answer2}}@endif" required>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="que_3">Security Q.3</label>
                                                <div class="col-md-9">
                                                    <select name="que_3" id="que_3" class="form-control">
                                                        <option value="">---Select---</option>
                                                        <option value="1" @if($isSecurityExist>0) {{$securityInfo->que_3 == 1 ? 'selected' : ''}} @endif>What was your high school mascot?</option>
                                                        <option value="2" @if($isSecurityExist>0) {{$securityInfo->que_3 == 2 ? 'selected' : ''}} @endif>Which is your favorite web browser?</option>
                                                        <option value="3" @if($isSecurityExist>0) {{$securityInfo->que_3 == 3 ? 'selected' : ''}} @endif>In what year was your father born?</option>
                                                        <option value="4" @if($isSecurityExist>0) {{$securityInfo->que_3 == 4 ? 'selected' : ''}} @endif>What is the name of your favorite childhood friend?</option>
                                                        <option value="5" @if($isSecurityExist>0) {{$securityInfo->que_3 == 5 ? 'selected' : ''}} @endif>What was your favorite food as a child?</option>
                                                    </select>
                                                    <input type="hidden" id="question3" name="question3" value="@if($isSecurityExist>0){{$securityInfo->question3}}@endif">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="answer3">Answer 3</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="answer3" name="answer3" value="@if($isSecurityExist>0){{$securityInfo->answer3}}@endif" required>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3 text-center">
                                                <div class="col-md-6">
                                                    @if(isset($isSecurityExist))
                                                        @if($isSecurityExist > 0)
                                                            <button type="submit" class="col-md-5 btn btn-primary text-center">Update</button>
                                                        @else
                                                            <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                                        @endif
                                                    @else
                                                        <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                                    @endif
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="reset" class=" col-md-5 btn btn-danger text-center">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-formation" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                        <form action="{{route('admin.storeFormation')}}" method="POST" name="storeFormation" enctype="multipart/form-data">
                                            @csrf
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    @foreach ($errors->all() as $error)
                                                        <div><i class="icon-warning-sign"></i> {{ $error }}</div>
                                                    @endforeach
                                                </div>
                                            @endif
                                            <input type="hidden" name="admin_id" value="{{Auth::user()->id}}">
                                            <input type="hidden" @if($isFormationExist > 0) name="formation_id" value="1" @endif>
                                            <h4 class="header-title text-center bg-icon-warning mb-1">Company Formation Information</h4>
                                            <hr/>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="fax_no">State of Formation</label>
                                                <div class="col-md-3">
                                                    @if($isFormationExist > 0)
                                                        <x-state data="state_of_formation" message="{{$formationInfo->state_of_formation}}"/>
                                                    @else
                                                        <x-state data="state_of_formation" message="GA"/>
                                                    @endif
                                                </div>
                                                <label class="col-md-3 col-form-label text-right" for="fax_no">Date of Incorporation</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control datepickers" id="date_of_incorporation" name="date_of_incorporation" value="@if($isFormationExist > 0){{date('d/m/Y',strtotime($formationInfo->date_of_incorporation))}}@endif">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="legal_name">Legal Name</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="legal_name" name="legal_name" value="@if($isProfileExsist > 0){{$companyInfo->company_name}}@endif" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="fax_no">Control Number</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="control_number" name="control_number" value="@if($isFormationExist > 0){{$formationInfo->control_number}}@endif">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" value="Active Compliance" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right"></label>
                                                <div class="custom-control custom-radio col-md-3 col-span-4">
                                                    @if($isFormationExist > 0)
                                                        @if($formationInfo->certificate != null)
                                                            <input type="radio" id="coporation" name="certificate_type" disabled value="1" class="custom-control-input" @if($isFormationExist > 0)@if($formationInfo->certificate_type == 1) checked @endif @endif">
                                                    @else
                                                        <input type="radio" id="coporation" name="certificate_type" value="1" class="custom-control-input" checked>
                                                    @endif
                                                    @else
                                                        <input type="radio" id="coporation" name="certificate_type" value="1" class="custom-control-input" checked>
                                                    @endif
                                                    <label class="custom-control-label" for="coporation">Corporation</label>
                                                </div>
                                                <div class="custom-control custom-radio  col-md-3">
                                                    @if($isFormationExist > 0)
                                                        @if($formationInfo->articles != null)
                                                            <input type="radio" id="llc" name="certificate_type" disabled value="2" class="custom-control-input" @if($isFormationExist > 0)@if($formationInfo->certificate_type == 2) checked @endif @endif">
                                                    @else
                                                        <input type="radio" id="llc" name="certificate_type" value="2" class="custom-control-input">
                                                    @endif
                                                    @else
                                                        <input type="radio" id="llc" name="certificate_type" value="2" class="custom-control-input">
                                                    @endif
                                                    <label class="custom-control-label" for="llc">LLC</label>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="certificate"><span id="certificateText">Certificate Of Corporation</span></label>
                                                <div class="col-md-9">
                                                    <label class="col-md-7 btn btn-primary" id="certificateLabel" data-src="Testing">Certificate Of Corporation</label>
                                                    @if($isFormationExist > 0)
                                                        @if($formationInfo->certificate != null)
                                                            <input type="hidden" name="certificate" class="form-control d-none" value="@if($isFormationExist > 0){{$formationInfo->certificate}}@endif">
                                                        @else
                                                            <input type="file" name="certificate" class="form-control d-none" id="certificate">
                                                        @endif
                                                    @else
                                                        <input type="file" name="certificate" class="form-control d-none" id="certificate">
                                                    @endif
                                                    <label class="col-md-3 btn btn-warning @if($isFormationExist > 0) @if($formationInfo->certificate != null) d-none @endif @endif" for="certificate">Browser...</label>
                                                    <label data-src="{{route('admin.removeCertificate')}}" data-token="{{csrf_token()}}" class="col-md-1 btn btn-danger @if($isFormationExist > 0) @if($formationInfo->certificate == null) d-none @endif @endif" id="certificateDelete"><i class="ri ri-delete-bin-2-line"></i></label>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="article"><span id="articleText">Article Of Corporation</span></label>
                                                <div class="col-md-9">
                                                    <label class="col-md-7 btn btn-primary" id="articlelabel" data-src="article">Article Of Corporation</label>
                                                    @if($isFormationExist > 0)
                                                        @if($formationInfo->articles != null)
                                                            <input type="hidden" name="articles" class="form-control d-none" value="@if($isFormationExist > 0){{$formationInfo->articles}}@endif">
                                                        @else
                                                            <input type="file" name="articles" class="form-control d-none" id="articles">
                                                        @endif
                                                    @else
                                                        <input type="file" name="articles" class="form-control d-none" id="articles">

                                                    @endif
                                                    <label class="col-md-3 btn btn-warning @if($isFormationExist > 0) @if($formationInfo->articles != null) d-none @endif @endif" for="articles">Browser...</label>
                                                    <label data-src="{{route('admin.removeCertificate')}}" data-token="{{csrf_token()}}" class="col-md-1 btn btn-danger @if($isFormationExist > 0) @if($formationInfo->articles == null) d-none @endif @endif" id="articlesDelete"><i class="ri ri-delete-bin-2-line"></i></label>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="agent_names">Agent Name</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" id="agent_names" name="agent_fname" value="@if($isFormationExist > 0){{$formationInfo->agent_fname}}@endif">
                                                </div>
                                                <div class="col-md-1">
                                                    <input type="text" class="form-control" id="agent_names" name="agent_mname" value="@if($isFormationExist > 0){{$formationInfo->agent_mname}}@endif">
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" id="agent_names" name="agent_lname" value="@if($isFormationExist > 0){{$formationInfo->agent_lname}}@endif">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="renewal_date">Renewal Date</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="renewal_date" name="renewal_date" value="@if($isFormationExist > 0){{date('d/m/Y',strtotime($formationInfo->renewal_date))}}@endif">
                                                </div>
                                                <div class="col-md-3">
                                                    <a type="submit" href="javascript:void(0)" class="btn btn-warning text-center col-md-12">Renew Record</a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a type="submit" href="javascript:void(0)" class="btn btn-warning text-center col-md-12">Renew Here</a>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3 text-center">
                                                <div class="col-md-6">
                                                    @if(isset($isFormationExist))
                                                        @if($isFormationExist > 0)
                                                            <button type="submit" class="col-md-5 btn btn-primary text-center">Update</button>
                                                        @else
                                                            <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                                        @endif
                                                    @else
                                                        <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                                    @endif
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="reset" class=" col-md-5 btn btn-danger text-center">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h4 class="header-title text-center bg-icon-warning mb-1">Shareholder / Officer Information</h4>
                                                <hr/>

                                                <div class="row">
                                                    <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                        <a href="javascript:void(0);" data-href="{{route('models.share-holder')}}" class="btn btn-warning waves-effect waves-light " data-toggle="modal" data-target="#modal-Share-holder-records">
                                                            <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Share Holder
                                                        </a>
                                                    </div>
                                                </div>

                                                <table id="Share-Holder-table" class="table table-bordered table-hover table-centered">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Name</th>
                                                        <th>Position</th>
                                                        <th>Percentage</th>
                                                        <th>Effective Date</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($shareHolders))
                                                        @foreach($shareHolders as $key=>$value)
                                                            <tr>
                                                                <td>{{$key+1}}</td>
                                                                <td>{{$value->fname." ".$value->mname." ".$value->lname}}</td>
                                                                <td>{{$value->position}}</td>
                                                                <td>{{$value->percentage}}%</td>
                                                                <td>{{date('d-m-Y',strtotime($value->effective_date))}}</td>
                                                                <td>
                                                                    @if($value->status == "active")
                                                                        <span class="badge badge-status badge-outline-success badge-pill">Active</span></td>
                                                                @elseif($value->status=="inactive")
                                                                    <span class="badge badge-status badge-outline-danger badge-pill">In-Active</span></td>
                                                                @endif
                                                                <td>
                                                                    <a href=""><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-license" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                        <form action="">
                                            <h4 class="header-title text-center bg-icon-warning mb-1">Business License</h4>
                                            <hr/>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="jurisdiction">Jurisdiction</label>
                                                <div class="col-md-3">
                                                    <select name="jurisdiction" id="jurisdiction" class="form-control">
                                                        <option value="">Select</option>
                                                        <option value="City">City</option>
                                                        <option value="County">County</option>
                                                    </select>
                                                </div>
                                                <label class="col-md-3 col-form-label show_license_city d-none text-right" for="license_city">City</label>
                                                <div class="col-md-3 show_license_city d-none">
                                                    <input type="text" class="form-control" id="license_city" name="license_city">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3 d-none" id="license_county_div">
                                                <label class="col-md-3 col-form-label text-right" for="license_county">County</label>
                                                <div class="col-md-3">
                                                    <select name="license_county" id="license_county" class="form-control">
                                                        <option value="Office">Office</option>
                                                        <option value="Mobile">Mobile</option>
                                                    </select>
                                                </div>
                                                <label class="col-md-3 col-form-label text-right" for="license_county_code">County Code</label>
                                                <div class="col-md-3">
                                                    <input type="text" readonly class="form-control" id="license_county_code" name="license_county_code">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="license_no">License</label>
                                                <div class="col-md-3">
                                                    <input type="text" readonly class="form-control" id="license_no" name="license_no">
                                                </div>
                                                <label class="col-md-3 col-form-label text-right" for="expire_date">Expire Date</label>
                                                <div class="col-md-3">
                                                    <input type="text" readonly class="form-control" id="expire_date" name="expire_date">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="license_note">Note</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="license_note" name="license_note">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3 text-center">
                                                <div class="col-md-6">
                                                    <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="reset" class=" col-md-5 btn btn-danger text-center">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-taxation" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                        <form action="">
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="fax_no">Username</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="fax_no" name="fax_no">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3 text-center">
                                                <div class="col-md-6">
                                                    <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="reset" class=" col-md-5 btn btn-danger text-center">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-banking" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                        <ul class="nav nav-pills navtab-bg nav-justified">
                                            <li class="nav-item">
                                                <a href="#bank" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-home-variant"></i></span>
                                                    <span class="d-none d-sm-inline-block">Bank Information</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#credit_card" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                                    <span class="d-none d-sm-inline-block">Credit Card</span>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane show active" id="bank">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                                        <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-Bank-records">
                                                                            <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Bank A/C Record
                                                                        </a>
                                                                    </div>
                                                                </div>

                                                                <table id="corporation-renewal-table" class="table table-bordered table-striped">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Bank Name</th>
                                                                        <th>A/C Nick Name</th>
                                                                        <th>Last 4 Digit A/C No</th>
                                                                        <th>Check Status</th>
                                                                        <th>OP Date</th>
                                                                        <th>Statement</th>
                                                                        <th>Staus</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                    <tr>
                                                                        <td>Tiger Nixon</td>
                                                                        <td>System Architect</td>
                                                                        <td>Edinburgh</td>
                                                                        <td>61</td>
                                                                        <td>2011/04/25</td>
                                                                        <td>$320,800</td>
                                                                        <td>2011/04/25</td>
                                                                        <td>$320,800</td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                            </div> <!-- end card body-->
                                                        </div> <!-- end card -->
                                                    </div><!-- end col-->
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="credit_card">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                                        <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-credit-card-records">
                                                                            <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Credit Card Record
                                                                        </a>
                                                                    </div>
                                                                </div>

                                                                <table id="corporation-renewal-table" class="table table-bordered table-striped">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Type of CC</th>
                                                                        <th>CC Issuer name</th>
                                                                        <th>Last 4 Digit A/C No</th>
                                                                        <th>Check Status</th>
                                                                        <th>OP Date</th>
                                                                        <th>Statement</th>
                                                                        <th>Staus</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                    <tr>
                                                                        <td>Tiger Nixon</td>
                                                                        <td>System Architect</td>
                                                                        <td>Edinburgh</td>
                                                                        <td>61</td>
                                                                        <td>2011/04/25</td>
                                                                        <td>$320,800</td>
                                                                        <td>2011/04/25</td>
                                                                        <td>$320,800</td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                            </div> <!-- end card body-->
                                                        </div> <!-- end card -->
                                                    </div><!-- end col-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-other" role="tabpanel" aria-labelledby="v-pills-company-tab">
                                        <form action="{{route('admin.storeProfile')}}" method="post" name="storeProfile" id="storeProfile">
                                            @csrf
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    @foreach ($errors->all() as $error)
                                                        <div><i class="icon-warning-sign"></i> {{ $error }}</div>
                                                    @endforeach
                                                </div>
                                            @endif
                                            <input type="hidden" value="{{\Illuminate\Support\Facades\Auth::user()->id}}" name="admin_id">
                                            <input type="hidden" @if(isset($isProfileExsist)) @if($isProfileExsist > 0) name=profile_id @endif @endif value="1">
                                            <h4 class="header-title text-center bg-icon-warning mb-1">Tech. Company Info</h4>
                                            <hr/>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="company_name_lbl">Company Name <span class="text-danger">*</span></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="VARR Tech LLC" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="business_name"> Business Name <span class="text-danger">*</span></label>
                                                <div class="col-md-9">
                                                    <input type="text" value="Vimbo" class="form-control" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_address_1">Reg Address1 <span class="text-danger">*</span></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="4550 Jimmy Carter Blvd." readonly>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_address_2">Reg Address2</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_city">City / state / zip <span class="text-danger">*</span></label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" value="Norcross" readonly>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" value="GA" readonly>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" value="30093" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-form-label col-md-3 text-right">Telephone No. <span class="text-danger">*</span></label>
                                                <div class="col-md-3">
                                                    <input type="text" value="Norcross" class="form-control " readonly/>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" value="Office" class="form-control " readonly/>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" value="" placeholder="Ext" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="fax_no">Fax no.</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="(770) 414-5351" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="website">Website</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="www.vimbo.com" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="email">Email</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="info@vimbo.com" readonly>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div> <!-- end col-->

                        </div> <!-- end row-->

                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('customModels')

    <!-- Add Bank Account  -->
    <!-- start -->
    <div class="modal fade" id="modal-Bank-records" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Bank Account Record</h4>
                    </div>
                    {{-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post">
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Bank Name</label>
                                    <div class="col-md-3">
                                        <select name="bank_name" id="bank_name" class="form-control">
                                            <option value="Office">Office</option>
                                            <option value="Mobile">Mobile</option>
                                        </select>
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Account Nick Name</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="agent_names" name="agent_mname">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Last 4 digit of A/C #</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="agent_names" name="agent_mname">
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Check Stubs</label>
                                    <div class="col-md-3">
                                        <select name="state_of_formation" id="state_of_formation" class="form-control">
                                            <option value="Office">Office</option>
                                            <option value="Mobile">Mobile</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Open Date</label>
                                    <div class="col-md-3">
                                        <input type="text" value="dd/mm/yyyy" class="form-control" id="agent_names" name="agent_mname">
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Statement</label>
                                    <div class="col-md-3">
                                        <select name="state_of_formation" id="state_of_formation" class="form-control">
                                            <option value="">select</option>
                                            <option value="E-statement">E-statement</option>
                                            <option value="Paper-Statement">Paper-Statement</option>
                                            <option value="FSC Get Online">FSC Get Online</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">status</label>
                                    <div class="col-md-3">
                                        <select name="state_of_formation" id="state_of_formation" class="form-control">
                                            <option value="active">Active</option>
                                            <option value="close">Close</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row mb-3 text-center">
                                    <div class="col-md-6">
                                        <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End -->

    <!-- Add Share Holders  -->
    <!-- start -->
    <div class="modal fade" id="modal-Share-holder-records" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Share Holder</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post" name="storeShareholder" id="storeShareholder">
                                @csrf
                                <input type="hidden" name="formation_id" value="1">
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="name">Name</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="name" name="fname">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" id="name" name="mname">
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="name" name="lname">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="position">Position</label>
                                    <div class="col-md-3">
                                        <select name="position" id="position" class="form-control">
                                            <option value="">Select</option>
                                            <option value="Agent">Agent</option>
                                            <option value="CEO">CEO</option>
                                            <option value="CFO">CFO</option>
                                            <option value="Secretary">Secretary</option>
                                            <option value="Sec">CEO / CFO / Sec.</option>
                                        </select>
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="percentage">Percentage</label>
                                    <div class="col-md-3">
                                        <input type="text" placeholder="Value In Percentage" class="form-control" id="percentage" name="percentage">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="effective_date">Effective Date</label>
                                    <div class="col-md-3">
                                        <input type="text" placeholder="dd/mm/yyyy" class="form-control datepickers" id="effective_date" name="effective_date">
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="SH_status">Status</label>
                                    <div class="col-md-3">
                                        <select name="status" id="SH_status" class="form-control">
                                            <option value="">select</option>
                                            <option value="active">Active</option>
                                            <option value="inactive">In-Active</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3 text-center">
                                    <div class="col-md-6">
                                        <a href="javascript:void(0);" data-src="{{route('admin.storeShareHolder')}}" class="addShareHolderEvent col-md-5 btn btn-primary text-center">Submit</a>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End -->

    <!-- Add Credit card Account  -->
    <!-- start -->
    <div class="modal fade" id="modal-credit-card-records" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Credit Card Account Record</h4>
                    </div>
                    {{-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post">
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Type of CC</label>
                                    <div class="col-md-3">
                                        <select name="state_of_formation" id="state_of_formation" class="form-control">
                                            <option value="Office">Office</option>
                                            <option value="Mobile">Mobile</option>
                                        </select>
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">CC Issuer name</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="agent_names" name="agent_mname">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Last 4 digit of A/C #</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="agent_names" name="agent_mname">
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Statement</label>
                                    <div class="col-md-3">
                                        <select name="state_of_formation" id="state_of_formation" class="form-control">
                                            <option value="Office">Office</option>
                                            <option value="Mobile">Mobile</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Open Date</label>
                                    <div class="col-md-3">
                                        <input type="text" value="dd/mm/yyyy" class="form-control" id="agent_names" name="agent_mname">
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">status</label>
                                    <div class="col-md-3">
                                        <select name="state_of_formation" id="state_of_formation" class="form-control">
                                            <option value="active">Active</option>
                                            <option value="close">Close</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Note</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="fax_no" name="fax_no">
                                    </div>
                                </div>


                                <div class="form-group row mb-3 text-center">
                                    <div class="col-md-6">
                                        <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                    </div>
                                </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- /.modal-content -->

@endsection

@section('scripts')
    <script src="{{asset('Backend/js/custom pages/admin_profile.js')}}"></script>
@endsection
