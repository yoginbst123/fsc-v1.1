@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Administrator</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Admin/Work Status</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-4">Admin Work Status</h4>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="nav flex-column nav-pills nav-pills-tab" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    <a class="nav-link active show mb-1" id="v-pills-corporation-tab" data-toggle="pill" href="#v-pills-corporation" role="tab" aria-controls="v-pills-home"
                                       aria-selected="true">
                                        Corporation</a>
                                    <a class="nav-link mb-1" id="v-pills-license-tab" data-toggle="pill" href="#v-pills-license" role="tab" aria-controls="v-pills-profile"
                                       aria-selected="false">
                                        License</a>
                                    <a class="nav-link mb-1" id="v-pills-taxation-tab" data-toggle="pill" href="#v-pills-taxation" role="tab" aria-controls="v-pills-messages"
                                       aria-selected="false">
                                        Taxation</a>
                                    <a class="nav-link mb-1" id="v-pills-banking-tab" data-toggle="pill" href="#v-pills-banking" role="tab" aria-controls="v-pills-settings"
                                       aria-selected="false">
                                        Banking</a>
                                    <a class="nav-link mb-1" id="v-pills-income-tab" data-toggle="pill" href="#v-pills-income" role="tab" aria-controls="v-pills-home"
                                       aria-selected="true">
                                        Income</a>
                                    <a class="nav-link mb-1" id="v-pills-expense-tab" data-toggle="pill" href="#v-pills-expense" role="tab" aria-controls="v-pills-profile"
                                       aria-selected="false">
                                        Expense</a>
                                    <a class="nav-link mb-1" id="v-pills-other-tab" data-toggle="pill" href="#v-pills-other" role="tab" aria-controls="v-pills-settings"
                                       aria-selected="false">
                                        Others</a>
                                </div>
                            </div> <!-- end col-->


                            <div class="col-sm-10">
                                <div class="tab-content pt-0">
                                    <div class="tab-pane fade active show" id="v-pills-corporation" role="tabpanel" aria-labelledby="v-pills-corporation-tab">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group row mb-3">
                                                    <label class="col-md-3 col-form-label text-right" for="reg_address">State</label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" value="GA" readonly>
                                                    </div>
                                                    <label class="col-md-3 col-form-label text-right" for="control">Control #</label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="control" value="2227" redonly>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-xl-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <ul class="nav nav-pills navtab-bg nav-justified">
                                                            <li class="nav-item">
                                                                <a href="#corporation_renewal" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-home-variant"></i></span>
                                                                    <span class="d-none d-sm-inline-block">Corporation Renewal</span>
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a href="#share_ledger" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                                                    <span class="d-none d-sm-inline-block">Share Ledger</span>
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a href="#amendment" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-email-variant"></i></span>
                                                                    <span class="d-none d-sm-inline-block">Amendment</span>
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a href="#minutes" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-cog"></i></span>
                                                                    <span class="d-none d-sm-inline-block">Minutes</span>
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a href="#docs" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                                                    <span class="d-none d-sm-inline-block">Docs</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div class="tab-pane show active" id="corporation_renewal">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="card">
                                                                            <div class="card-body">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                                                        <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-corporation-records">
                                                                                            <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Corporation Renewal Record
                                                                                        </a>
                                                                                    </div>
                                                                                </div>

                                                                                <table id="corporation-renewal-table" class="table table-responsive table-bordered table-striped">
                                                                                    <thead>
                                                                                    <tr class="text-center">
                                                                                        <th>Renewal Year</th>
                                                                                        <th>Renewal For</th>
                                                                                        <th>Paid Amount</th>
                                                                                        <th>Payment Method</th>
                                                                                        <th>Corporation Status</th>
                                                                                        <th>SOS Receipt</th>
                                                                                        <th>Annul Officer</th>
                                                                                        <th width="7%">Action</th>
                                                                                    </tr>
                                                                                    </thead>

                                                                                    <tbody>
                                                                                    @if(isset($corporationLicense))
                                                                                        @foreach($corporationLicense as $key=>$license)
                                                                                            <tr class="text-center">
                                                                                                <td>{{$license->renewal_year}}</td>
                                                                                                <td>{{$license->renewal_for}}</td>
                                                                                                <td>{{$license->paid_amount}}</td>
                                                                                                <td>{{$license->method_of_payment}}</td>
                                                                                                <td>{{$license->corporation_status}}</td>
                                                                                                <td><x-actionlink route="" id="" class="" buttons="view" /></td>
                                                                                                <td><x-actionlink route="" id="" class="" buttons="view" /></td>
                                                                                                <td>
                                                                                                    <x-actionlink route="" id="" class="edit_corporation" buttons="edit" />
                                                                                                    <x-actionlink route="" id="" class="" buttons="delete" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        @endforeach
                                                                                    @endif

                                                                                    </tbody>
                                                                                </table>

                                                                            </div> <!-- end card body-->
                                                                        </div> <!-- end card -->
                                                                    </div><!-- end col-->
                                                                </div>
                                                            </div>

                                                            <div class="tab-pane" id="share_ledger">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="card">
                                                                            <div class="card-body">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                                                        <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-share-Ledger-records">
                                                                                            <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Record
                                                                                        </a>
                                                                                    </div>
                                                                                </div>

                                                                                <table id="corporation-renewal-table" class="table table-bordered table-striped">
                                                                                    <thead>
                                                                                    <tr class="text-center">
                                                                                        <th>ID</th>
                                                                                        <th>NAME OF CERTIFICATE HOLDER</th>
                                                                                        <th>CERTIFICATES ISSUED NO. SHARES*</th>
                                                                                        <th>FROM WHOM TRANSFERRED (If Original Issue Enter As Such)</th>
                                                                                        <th>AMOUNT PAID THEREON</th>
                                                                                        <th>DATE OF TRANSFER OF SHARES*</th>
                                                                                        <th>CERTIFICATES SURRENDERED CERTIF. NOS.</th>
                                                                                        <th>CERTIFICATES SURRENDERED NO. SHARES*</th>
                                                                                        <th width="7%">Action</th>
                                                                                    </tr>
                                                                                    </thead>

                                                                                    <tbody>
                                                                                    <tr class="text-center">
                                                                                        <td>Tiger Nixon</td>
                                                                                        <td>System Architect</td>
                                                                                        <td>Edinburgh</td>
                                                                                        <td>61</td>
                                                                                        <td>2011/04/25</td>
                                                                                        <td>$320,800</td>
                                                                                        <td>2011/04/25</td>
                                                                                        <td>$320,800</td>
                                                                                        <td>$320,800</td>
                                                                                        <td>
                                                                                            <x-actionlink route="" id="" class="" buttons="edit" />
                                                                                            <x-actionlink route="" id="" class="" buttons="delete" />
                                                                                        </td>

                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>

                                                                            </div> <!-- end card body-->
                                                                        </div> <!-- end card -->
                                                                    </div><!-- end col-->
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="amendment">
                                                                <p>Amendment</p>
                                                            </div>
                                                            <div class="tab-pane" id="minutes">
                                                                <p>Minutes</p>
                                                            </div>
                                                            <div class="tab-pane" id="docs">
                                                                <form action="" class="control-form" method="post">
                                                                    <div class="form-group row mb-3">
                                                                        <label class="col-md-2 col-form-label text-right" for="work_status_docs">Document Name <span class="text-danger">*</span></label>
                                                                        <div class="col-md-3">
                                                                            <select name="work_status_docs[]" id="work_status_docs" class="form-control">
                                                                                <option value="Office">Office</option>
                                                                                <option value="Mobile">Mobile</option>
                                                                            </select>
                                                                        </div>
                                                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#bs-example-modal-lg"><i class="ri ri-2x ri-add-box-line btn-warning"></i> </a>
                                                                        <label class="col-md-2 col-form-label text-right" for="certificate"><span id="certificateText">Certificate of Corporation</span></label>
                                                                        <div class="col-md-3">
                                                                            <input type="file" name="certificate" class="form-control" id="certificate">
                                                                        </div>
                                                                        <button type="submit" class="col-md-1 btn btn-primary text-center">Submit</button>
                                                                    </div>

                                                                </form>

                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <table id="corporation-renewal-table" class="table table-responsive table-bordered table-striped">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>ID</th>
                                                                                <th>Document Name</th>
                                                                                <th>view</th>
                                                                                <th width="7%">Action</th>
                                                                            </tr>
                                                                            </thead>

                                                                            <tbody>
                                                                            <tr>
                                                                                <td>Tiger Nixon</td>
                                                                                <td>System Architect</td>
                                                                                <td>Edinburgh</td>
                                                                                <td>61</td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div><!-- end col-->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> <!-- end card -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-license" role="tabpanel" aria-labelledby="v-pills-license-tab">
                                        <div class="row">
                                            <div class="col-md-12 col-xl-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <ul class="nav nav-pills navtab-bg nav-justified">
                                                            <li class="nav-item">
                                                                <a href="#business_license" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-home-variant"></i></span>
                                                                    <span class="d-none d-sm-inline-block">Business License</span>
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a href="#professional_license" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                                                    <span class="d-none d-sm-inline-block">Professional License</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div class="tab-pane show active" id="business_license">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="card">
                                                                            <div class="card-body">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                                                        <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-Business-License-records">
                                                                                            <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Business License
                                                                                        </a>
                                                                                    </div>
                                                                                </div>

                                                                                <table id="corporation-renewal-table" class="table table-responsive table-bordered table-hover table-centered">
                                                                                    <thead>
                                                                                    <tr class="text-center">
                                                                                        <th>Year</th>
                                                                                        <th>License Gross</th>
                                                                                        <th>License Fee</th>
                                                                                        <th>License #</th>
                                                                                        <th>License Issue Date</th>
                                                                                        <th>License Copy</th>
                                                                                        <th>Status</th>
                                                                                        <th width="7%">Action</th>
                                                                                        <th>Form</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    @php $dateutility = new \App\Utility\DateUtility(); @endphp
                                                                                    @if(isset($businessLicense))
                                                                                        @foreach($businessLicense as $key=>$license)
                                                                                            <tr class="text-center">
                                                                                                <td>{{$license->year}}</td>
                                                                                                <td>{{$license->gross_revenue}}</td>
                                                                                                <td>{{$license->license_fee}}</td>
                                                                                                <td>{{$license->certificate_no}}</td>
                                                                                                <td>{{$dateutility->retriveDate($license->license_issue_date)}}</td>
                                                                                                <td class="text-center"><x-actionlink route="" id="" class="" buttons="view" /></td>
                                                                                                <td><x-status type="{{$license->license_status}}" /></td>
                                                                                                <td>
                                                                                                    <x-actionlink route="" id="" class="" buttons="edit" />
                                                                                                    <x-actionlink route="" id="" class="" buttons="delete" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <x-actionlink route="" id="" class="btn btn-primary" buttons="Create" />
                                                                                                </td>

                                                                                            </tr>
                                                                                        @endforeach
                                                                                        @endif

                                                                                    </tbody>
                                                                                </table>

                                                                            </div> <!-- end card body-->
                                                                        </div> <!-- end card -->
                                                                    </div><!-- end col-->
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="professional_license">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="card">
                                                                            <div class="card-body">
                                                                                <p>Professional License</p>
                                                                            </div> <!-- end card body-->
                                                                        </div> <!-- end card -->
                                                                    </div><!-- end col-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> <!-- end card -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end col-->

                        </div> <!-- end row-->

                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('customModels')
    <!--  Modal content for the Large example -->
    <div class="modal fade" id="bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title text-center" id="myLargeModalLabel">Document Name</h4>
                    </div>

                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post">
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="certificate"><span id="certificateText">Document name</span></label>
                                    <div class="col-md-9">
                                        <input type="text" name="document_name" class="form-control" id="document_names">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-primary" id="create_document_name">Submit</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xl-12 col-sm-12">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <td>ID</td>
                                    <td>Document Name</td>
                                    <td>Action</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Docs</td>
                                    <td>
                                        <a href="javascript:void(0);"><i class="ri ri-2x ri-pencil-line text-success"></i></a>
                                        <a href="javascript:void(0);"><i class="ri ri-2x ri-delete-bin-2-line text-danger"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Add Corporation Renewal Records -->
    <!-- start -->
    <div class="modal fade" id="modal-corporation-records" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Corporation Renew Record</h4>
                    </div>
                    {{--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form parsley-examples" action="{{route('admin.storeCorporationLicense')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="admin_id" value="1"/>
                                <input type="hidden" name="corporation_id" id="corporation_id" />
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="renewal_for"><span id="certificateText">Renew For </span></label>
                                    <div class="col-md-3">
                                        <select name="renewal_for" id="renewal_for" class="form-control">
                                            <option value="1">1 Year</option>
                                            <option value="2">2 Year</option>
                                            <option value="3">3 Year</option>
                                        </select>
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="renewal_year"><span id="certificateText">Renewal Year</span></label>
                                    <div class="col-md-3">
                                        <input type="text" name="renewal_year" class="form-control" id="renewal_year" readonly>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="renewal_amount"><span id="certificateText">Renew Amount</span></label>
                                    <div class="col-md-3 input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-dollar-sign"></i></span>
                                        </div>
                                        <input type="text" name="renewal_amount" class="form-control" id="renewal_amount" readonly>
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="paid_amount"><span id="certificateText">Paid Amount</span></label>
                                    <div class="col-md-3 input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-dollar-sign"></i></span>
                                        </div>
                                        <input type="text" name="paid_amount" class="form-control" id="paid_amount" required>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="method_of_payment"><span id="certificateText">Method Of Payment</span></label>
                                    <div class="col-md-9">
                                        <input type="text" name="method_of_payment" class="form-control" id="method_of_payment">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="corporation_status"><span id="certificateText">Status</span></label>
                                    <div class="col-md-9">
                                        <select name="corporation_status" id="corporation_status" class="form-control" required>
                                            <option value="">Select</option>
                                            <option value="Active Owes Curr. Yr. AR">Active Owes Curr. Yr. AR</option>
                                            <option value="Active Compliance">Active Compliance</option>
                                            <option value="Active Noncompliance">Active Noncompliance</option>
                                            <option value="Admin. Dissolved">Admin. Dissolved</option>
                                            <option value="Dis-Cancel-Termin">Dis-Cancel-Termin</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="annually_receipt"><span id="certificateText">Annually Receipt</span></label>
                                    <div class="col-md-9">
                                        <input type="file" name="annually_receipt" class="form-control" id="annually_receipt" required>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="officer"><span id="certificateText">Officer</span></label>
                                    <div class="col-md-9">
                                        <input type="file" name="officer" class="form-control" id="officer" required>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="website"><span id="certificateText">Website</span></label>
                                    <div class="col-md-9">
                                        <input type="text" name="website" class="form-control" id="website">
                                    </div>
                                </div>
                                <div class="form-group row mb-3 text-center">
                                    <div class="col-md-6">
                                        <input type="submit" value="submit" class="col-md-5 btn btn-primary text-center">
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End -->

    <!-- Add Record of share Ledger -->
    <!-- start -->
    <div class="modal fade" id="modal-share-Ledger-records" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Share Ledger Record</h4>
                    </div>
                    {{--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form shareLedger" method="post">
                                <div class="form-group row mb-3">
                                    <label class="col-md-5 col-form-label text-right" for="holder_name">
                                        <span id="certificateText">Name of Certificate of Holder</span>
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="holder_name" name="holder_name" placeholder="Holder Name" required="">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-5 col-form-label text-right" for="issue_certificate">
                                        <span id="certificateText">Certificates issued No. Shares</span>
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="issue_certificate" name="issue_certificate" placeholder="Certificates Issue No of Shares" required="">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-5 col-form-label text-right" for="origine_issue_from">
                                        <span id="certificateText">From whom Transferred (If Original issue Enter as such)</span>
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="origine_issue_from" name="origine_issue_from" placeholder="From Transfer">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-5 col-form-label text-right" for="paid_amount">
                                        <span id="certificateText">Amount paid thereon</span>
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control txtinput_1" id="paid_amount" name="paid_amount" placeholder="Paid Amount">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-5 col-form-label text-right" for="transfer_date">
                                        <span id="certificateText">Date of Transfer of Shares</span>
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control ledgerdate datepickers" id="transfer_date" name="transfer_date" placeholder="mm/dd/yyyy" readonly="" required="">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-5 col-form-label text-right" for="surrender_certificate">
                                        <span id="certificateText">Certificates Surrendered certif. Nos. </span>
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="surrender_certificate" name="surrender_certificate" placeholder="Surrender Certificate No">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-5 col-form-label text-right" for="surrender_certificate_no">
                                        <span id="certificateText">Certificates Surrendered No. Shares</span>
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="surrender_certificate_no" name="surrender_certificate_no" placeholder="Surrender Certificate No of Shares" required="">
                                    </div>
                                </div>


                                <div class="form-group row mb-3 text-center">
                                    <div class="col-md-6">
                                        <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End -->

    <!-- Add Business License  -->
    <!-- start -->
    <div class="modal fade" id="modal-Business-License-records" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Business License Record</h4>
                    </div>
                    {{--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" action="{{route('admin.storeBusinessLicense')}}" method="post" id="businessLicense" name="businessLicense" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="admin_id" value="{{Auth::user()->id}}">
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="year"><span id="certificateText">Year </span></label>
                                    <div class="col-md-3">
                                        <select name="year" id="year" class="form-control" required>
                                            <option value="">Select</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                        </select>
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="gross_revenue"><span id="certificateText">Gross Revenue</span></label>
                                    <div class="col-md-3">
                                        <input type="text" name="gross_revenue" class="form-control" id="gross_revenue" required>
                                    </div>
                                </div>

                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="year"><span id="license_fee">License Fee </span></label>
                                    <div class="col-md-3">
                                        <input type="text" name="license_fee" class="form-control" id="license_fee" required>
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="license_status"><span id="certificateText">License Status</span></label>
                                    <div class="col-md-3">
                                        <select name="license_status" id="license_status" class="form-control">
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                            <option value="Hold">Hold</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="license_copy"><span id="license_fee">License Copy </span></label>
                                    <div class="col-md-9">
                                        <input type="file" name="license_copy" class="form-control" id="license_copy">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="year"><span id="license_issue_date">License Issue Date </span></label>
                                    <div class="col-md-3">
                                        <input type="text" name="license_issue_date" class="form-control datepickers" id="license_issue_date" required>
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="certificate_no"><span id="certificateText">Certificate No</span></label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" name="certificate_no" id="certificate_no">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="license_website"><span id="license_fee">Website</span></label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="http:// or https:// required" name="license_website" class="form-control" id="license_website">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="license_note"><span id="license_fee">Note </span></label>
                                    <div class="col-md-9">
                                        <input type="text" name="license_note" class="form-control" id="license_note">
                                    </div>
                                </div>

                                <div class="form-group row mb-3 text-center">
                                    <div class="col-md-6">
                                        <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End -->

@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="{{asset('Backend/js/custom pages/workstatus.js')}}"></script>
@endsection
