@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Administrator</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Admin/Upload</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-4">Admin Upload Documents</h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-uploads-documents">
                                                    <span class="btn-label"><i class="mdi mdi-plus"></i></span>Upload New Document
                                                </a>
                                            </div>
                                        </div>

                                        <table id="upload-table" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Document Name</th>
                                                <th>Upload</th>
                                                <th>Expire Date</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>System Architect</td>
                                                <td>Certificate.pdf</td>
                                                <td>Mar/01/2021</td>
                                                <td>
                                                    <a href=""><i class="ri f-1-4x ri-edit-box-line text-primary"></i></a>
                                                    <a href=""><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>

                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('customModels')

    <!-- Upload New Docs  -->
    <!-- start -->
    <div class="modal fade" id="modal-uploads-documents" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Upload Document</h4>
                    </div>
                    {{--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post">
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="upload_name"><span id="certificateText">Upload Name</span></label>
                                    <div class="col-md-9">
                                        <select name="upload_name" id="upload_name" class="form-control">
                                            <option value=""> Select</option>
                                            <option value="SOS Certificate"> SOS Certificate</option>
                                            <option value="SOS-AOI"> SOS-AOI</option>
                                            <option value="County">Business License County Bacon </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="upload_docs"><span id="certificateText">Upload</span></label>
                                    <div class="col-md-9">
                                        <input type="file" name="upload_docs" class="form-control" id="upload_docs">
                                    </div>
                                </div>

                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="renewal_year">Renewal</label>
                                    <div class="col-md-3">
                                        <input type="text" name="renewal_year" class="form-control" id="renewal_year">
                                    </div>
                                    <label class="col-md-1 col-form-label text-info">Year</label>
                                    <label class="col-md-2 col-form-label text-right" for="renewal_date">Renewal Date</label>
                                    <div class="col-md-3">
                                        <input type="text" name="renewal_date" class="form-control" id="renewal_date" readonly>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="license_copy"><span id="license_fee">Days</span></label>
                                    <div class="col-md-3">
                                        <input type="text" name="r_day" placeholder="Reminder" class="form-control" id="r_day">
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" name="n_day" placeholder="Notification" class="form-control" id="n_day">
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" name="w_day" placeholder="warning" class="form-control" id="w_day">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="name">Name</label>
                                    <div class="col-md-3">
                                        <input type="text" name="name" class="form-control" id="name">
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="certificate_no">Telephone</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" name="telephone" id="telephone">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="website"><span id="license_fee">Website Link</span></label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="http:// or https:// required" name="website" class="form-control" id="website">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="main_website">Main Website</label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="http:// or https:// required" name="main_website" class="form-control" id="main_website">
                                    </div>
                                </div>


                                <div class="form-group row mb-3 text-center">
                                    <div class="col-md-6">
                                        <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End -->

@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="{{asset('Backend/js/custom pages/uploads.js')}}"></script>
@endsection
