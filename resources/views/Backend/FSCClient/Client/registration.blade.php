@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Employee / User</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Employee / Hiring Ads.</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-4">Hiring</h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <table id="new-registration-table" class="table table-bordered">
                                            <thead>
                                            <tr>
                                            <tr>
                                                <th>No</th>
                                                <th>Inquiry Rec'd Dt</th>
                                                <th>Company Name</th>
                                                <th>Type of Business</th>
                                                <th>Contact Name</th>
                                                <th width="12%;">Telephone #</th>
                                                <th>Action</th>
                                            </tr>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Mar/01/2021</td>
                                                <td>Jk Software</td>
                                                <td>Personal</td>
                                                <td>Ankit Patel </td>
                                                <td>(999)999-9999</td>
                                                <td>
                                                    <a href=""><i class="ri f-1-4x ri-edit-box-line text-primary"></i></a>
                                                    <a href=""><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>

                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('customModels')
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="{{asset('Backend/js/custom pages/fsc_client.js')}}"></script>
@endsection
