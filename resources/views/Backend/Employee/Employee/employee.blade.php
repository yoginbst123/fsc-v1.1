@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Employee / User</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Employee / Employee</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-4">List Of Employees</h4>



                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                <a href="{{route('employee.create')}}" class="btn btn-warning waves-effect waves-light">
                                                    <span class="btn-label"><i class="mdi mdi-plus"></i></span>New EE / User
                                                </a>
                                            </div>
                                        </div>

                                        <table id="new-application-table" class="table table-bordered">
                                            <thead>
                                            <tr>
                                            <tr>
                                                <th>No</th>
                                                <th>Type</th>
                                                <th>EE / User ID</th>
                                                <th>Employee Name</th>
                                                <th>Email ID</th>
                                                <th>Telephone</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Employee</td>
                                                <td>FSC-EMP-001</td>
                                                <td>Ankit Patel</td>
                                                <td>admin@fsc.com</td>
                                                <td>(789)456-1230</td>
                                                <td><span class="badge badge-status badge-outline-success badge-pill">Active</span></td>
                                                <td>
                                                   <x-actionlink route="{{route('admin.editEmployee')}}" id="" class="" buttons="edit" />
                                                   <x-actionlink route="" id="" class="" buttons="delete" />
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>

                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('customModels')

    <!-- Upload New Docs  -->
    <!-- start -->
    <div class="modal fade" id="modal-new-employee" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">New Employee</h4>
                    </div>
                    {{--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post">
                                <div class="form-group row mb-3">
                                    <label class="col-form-label col-md-3 text-right">Posting Date :</label>
                                    <div class="col-md-3">
                                        <input name="post_date" type="text" id="post_date" value="{{\Carbon\Carbon::now()}}" class="form-control">
                                    </div>
                                    <label class="col-form-label col-md-3 text-right">Job type :</label>
                                    <div class="col-md-3">
                                        <select class="form-control" name="job_type" id="job_type">
                                            <option value="">Select</option>
                                            <option value="Full Time">Full Time</option>
                                            <option value="Part Time">Part Time</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="position_name">Position Name</label>
                                    <div class="col-md-9">
                                        <input type="text" name="position_name" class="form-control" id="position_name">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right">Country / State / City</label>
                                    <div class="col-md-3">
                                        <select class="form-control" name="country" id="country">
                                            <option value="">Select</option>
                                            <option value="IND">IND</option>
                                            <option value="USA">USA</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control" name="states" id="states">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" name="city" placeholder="warning" class="form-control" id="city">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="name">Name</label>
                                    <div class="col-md-3">
                                        <input type="text" name="name" class="form-control" id="name">
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="certificate_no">Telephone</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" name="telephone" id="telephone">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="description">Description</label>
                                    <div class="col-md-9">
                                        <textarea id="description" name="description" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row mb-3 text-center">
                                    <div class="col-md-6">
                                        <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End -->

@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="{{asset('Backend/js/custom pages/employee.js')}}"></script>
@endsection
