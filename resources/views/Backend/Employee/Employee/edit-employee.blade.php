@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Admin Profile</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Admin/Profile</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h3 class="header-title text-center mb-4">Edit Employee / User</h3>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col col-md-10">
                                <div class="card">
                                    <div class="card-body">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('scripts')
<script>
$(document).ready(function(){

});
</script>
    <script src="{{asset('Backend/js/custom pages/admin_profile.js')}}"></script>
@endsection
