@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Admin Profile</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Admin/Profile</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h3 class="header-title text-center mb-4">New Employee / User</h3>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col col-md-10">
                                <div class="card">
                                    <div class="card-body">
                                        <form action="" method="POST" id="create-employee" name="create_employee">
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right">Role</label>
                                                <div class="custom-control custom-radio col-md-3 mt-1 col-span-4">
                                                    <input type="radio" id="employee" name="roles" value="1" class="custom-control-input mt-1" checked="">
                                                    <label class="custom-control-label" for="employee">Employee</label>
                                                </div>
                                                <div class="custom-control custom-radio mt-1 col-md-3">
                                                    <input type="radio" id="User" name="roles" value="2" class="custom-control-input mt-1">
                                                    <label class="custom-control-label" for="User">User</label>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="employee_id">Employee ID / Represent <span class="text-danger">*</span></label>
                                                <div class="col-md-3">
                                                    <input type="text" name="employee_id" class="form-control" id="employee_id">
                                                </div>
                                                <div class="col-md-5">
                                                    <select name="team" id="team" class="form-control">
                                                        <option value="Office">Office</option>
                                                        <option value="Mobile">Mobile</option>
                                                    </select>
                                                </div>
                                                <a class="text-warning" href="javascript:void(0);" data-toggle="modal" data-target="#bs-example-modal-lg"><i class="ri ri-2x ri-add-box-line btn-warning"></i> </a>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_city">Name</label>
                                                <div class="col-md-2">
                                                    <select name="prefix_name" id="prefix_name" class="form-control">
                                                        <option value="Mr">Mr.</option>
                                                        <option value="Mrs">Mrs.</option>
                                                        <option value="Miss">Miss.</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="name" name="fname">
                                                </div>
                                                <div class="col-md-1">
                                                    <input type="text" class="form-control" id="name" name="mname">
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="reg_city" name="lname">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_address">Address1</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="reg_address" name="address_1">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_address">Address2</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="reg_address" name="address_2">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_city">City / state / zip</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="reg_city" name="city">
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="reg_city" name="state">
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="reg_city" name="zipcode">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-form-label col-md-3 text-right">Telephone No. <span class="text-danger">*</span></label>
                                                <div class="col-md-3">
                                                    <input name="contact_no_1" value="" type="tel" id="contact_no_1" class="form-control" placeholder="(999) 999-9999"/>
                                                </div>
                                                <div class="col-md-3">
                                                    <select name="contact_no_type_1" id="contact_no_type_1" class="form-control">
                                                        <option value="Office">Office</option>
                                                        <option value="Mobile">Mobile</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input class="form-control" id="contact_no_extension_1" maxlength="5" name="contact_no_extension_1" value="" placeholder="Ext" type="text">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-form-label col-md-3 text-right">Telephone No 2. <span class="text-danger">*</span></label>
                                                <div class="col-md-3">
                                                    <input name="contact_no_2" value="" type="tel" id="contact_no_2" class="form-control" placeholder="(999) 999-9999"/>
                                                </div>
                                                <div class="col-md-3">
                                                    <select name="contact_no_type_2" id="contact_no_type_2" class="form-control">
                                                        <option value="Office">Office</option>
                                                        <option value="Mobile">Mobile</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input class="form-control" id="contact_no_extension_2" maxlength="5" name="contact_no_extension_2" value="" placeholder="Ext" type="text">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="fax_no">Fax no.</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="fax_no" name="fax_no">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="email">Email</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="email" name="email">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="profile">Profile</label>
                                                <div class="col-md-9">
                                                    <input type="file" class="form-control" id="profile" name="profile">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3 text-center">
                                                <div class="col-md-6">
                                                    <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="{{route('employee.employees')}}" type="reset" class=" col-md-5 btn btn-danger text-center">Cancel</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('scripts')
    <script src="{{asset('Backend/js/custom pages/admin_profile.js')}}"></script>
@endsection
