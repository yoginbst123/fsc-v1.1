<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>@yield('titles')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A Fully Responsive Financial Service Center where User Can Manage Accounts." name="description"/>
    <meta content="Vimbo" name="author"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('Backend/images/favicon.png')}}">

    <!-- plugin css -->
    <link href="{{asset('Backend/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.css')}}" rel="stylesheet" type="text/css"/>

    <!-- App css -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

    <link href="{{asset('Backend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" id="bs-default-stylesheet"/>
    <link href="{{asset('Backend/css/app.min.css')}}" rel="stylesheet" type="text/css" id="app-default-stylesheet"/>

    <!-- icons -->
    <link href="{{asset('Backend/css/icons.min.css')}}" rel="stylesheet" type="text/css"/>

</head>

<body class="loading">

<!-- Begin page -->
<div id="wrapper">

    <!-- ========== Left Sidebar Start ========== -->
    <div class="left-side-menu">

        <!-- LOGO -->
        <div class="logo-box">
            <a href="index.html" class="logo logo-light text-center">
                <span class="logo-sm">
                    <img src="{{asset('Backend/images/favicon.png')}}" alt="" height="65">
                </span>
                <span class="logo-lg">
                    <img src="{{asset('Backend/images/fsc_logo.png')}}" alt="" height="60">
                </span>
            </a>
        </div>

        <div class="h-100" data-simplebar>

            <div id="sidebar-menu">

                <ul id="side-menu">

                    <li class="menu-title">Navigation</li>

                    <li>
                        <a href="#sidebarDashboards" data-toggle="collapse" class="waves-effect">
                            <i class="ri-dashboard-line"></i>
                            <span class="badge badge-success badge-pill float-right">3</span>
                            <span> Dashboards </span>
                        </a>
                        <div class="collapse" id="sidebarDashboards">
                            <ul class="nav-second-level">
                                <li>
                                    <a href="index.html">Sales</a>
                                </li>
                                <li>
                                    <a href="dashboard-crm.html">CRM</a>
                                </li>
                                <li>
                                    <a href="dashboard-analytics.html">Analytics</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li>
                        <a href="#sidebarAdmin" data-toggle="collapse">
                            <i class="ri-profile-line"></i>
                            <span> Administrator </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse" id="sidebarAdmin">
                            <ul class="nav-second-level">
                                <li>
                                    <a href="{{route('admin.work-status')}}">Work Status</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">CE Status</a>
                                </li>
                                <li>
                                    <a href="{{route('admin.uploads')}}">Uploads</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li>
                        <a href="#sidebarEmployeeUser" data-toggle="collapse">
                            <i class="ri-group-line"></i>
                            <span> Employee / User </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse" id="sidebarEmployeeUser">
                            <ul class="nav-second-level">
                                <li>
                                    <a href="{{route('employee.hiring')}}">Hiring Ad</a>
                                </li>
                                <li>
                                    <a href="{{route('employee.application')}}">Application Records</a>
                                </li>
                                <li>
                                    <a href="{{route('employee.candidate')}}">Candidate Data</a>
                                </li>
                                <li>
                                    <a href="{{route('employee.employees')}}">Add / Edit-EE/User</a>
                                </li>
                                <li>
                                    <a href="{{route('employee.supervisor')}}">Supervisor Code</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li>
                        <a href="#sidebarFSCClient" data-toggle="collapse">
                            <i class="ri-user-4-line"></i>
                            <span> FSC Client </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse" id="sidebarFSCClient">
                            <ul class="nav-second-level">
                                <li>
                                    <a href="javascript:void(0);">Client Registration</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">Add / Edit - FSC Client</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li>
                        <a href="#sidebarClientManagement" data-toggle="collapse">
                            <i class="ri-account-box-line"></i>
                            <span> Client Management</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse" id="sidebarClientManagement">
                            <ul class="nav-second-level">
                                <li>
                                    <a href="javascript:void(0);">Edit / View - Client</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li>
                        <a href="#sidebarWorkManagement" data-toggle="collapse">
                            <i class="ri-clipboard-line"></i>
                            <span>Work Management</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse" id="sidebarWorkManagement">
                            <ul class="nav-second-level">
                                <li>
                                    <a href="javascript:void(0);">Today's Work</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">New</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">Regular</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">To Do</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">Records</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">Status</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">Submission</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">Uploads</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">Edit / View Service</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">Task</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">Appointment</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li>
                        <a href="#sidebarSetups" data-toggle="collapse">
                            <i class="ri-settings-2-line"></i>
                            <span> Setups </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse" id="sidebarSetups">
                            <ul class="nav-second-level">
                                <li>
                                    <a href="#sidebarBanks" data-toggle="collapse">
                                        <i class="ri-bank-line"></i>
                                        <span>Bank / Vendor Setup</span> <span class="menu-arrow"></span>
                                    </a>
                                    <div class="collapse" id="sidebarBanks">
                                        <ul class="nav-second-level">
                                            <li>
                                                <a href="{{route('setup.bank-list')}}">Banks</a>
                                            </li>
                                            <li>
                                                <a href="javascript: void(0);">Vendors</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </li>

                </ul>

            </div>
            <!-- End Sidebar -->

            <div class="clearfix"></div>

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <div class="content">

            <!-- Topbar Start -->
            <div class="navbar-custom">
                <div class="container-fluid">

                    <ul class="list-unstyled topnav-menu float-right mb-0">

                        <li class="d-none d-lg-block">
                            <form class="app-search">
                                <div class="app-search-box dropdown">
                                    <div class="input-group">
                                        <input type="search" class="form-control" placeholder="Search..." id="top-search">
                                        <div class="input-group-append">
                                            <button class="btn" type="submit">
                                                <i class="fe-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="dropdown-menu dropdown-lg" id="search-dropdown">
                                        <!-- item-->
                                        <div class="dropdown-header noti-title">
                                            <h5 class="text-overflow mb-2">Found <span class="text-danger">09</span> results</h5>
                                        </div>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <i class="fe-home mr-1"></i>
                                            <span>Analytics Report</span>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <i class="fe-aperture mr-1"></i>
                                            <span>How can I help you?</span>
                                        </a>
                                        <!-- item-->
                                        <div class="dropdown-header noti-title">
                                            <h6 class="text-overflow mb-2 text-uppercase">Users</h6>
                                        </div>

                                        <div class="notification-list">

                                            <!-- item-->
                                            <a href="javascript:void(0);" class="dropdown-item notify-item">
                                                <div class="media">
                                                    <img class="d-flex mr-2 rounded-circle" src="{{asset('Backend/images/users/avatar-5.jpg')}}" alt="Generic placeholder image" height="32">
                                                    <div class="media-body">
                                                        <h5 class="m-0 font-14">Jacob Deo</h5>
                                                        <span class="font-12 mb-0">Developer</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </li>

                        <li class="dropdown d-inline-block d-lg-none">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <i class="fe-search noti-icon"></i>
                            </a>
                            <div class="dropdown-menu dropdown-lg dropdown-menu-right p-0">
                                <form class="p-3">
                                    <input type="text" class="form-control" placeholder="Search ..." aria-label="Recipient's username">
                                </form>
                            </div>
                        </li>

                        <li class="dropdown d-none d-lg-inline-block topbar-dropdown">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <i class="fe-grid noti-icon"></i>
                            </a>
                            <div class="dropdown-menu dropdown-lg dropdown-menu-right p-0">

                                <div class="p-2">
                                    <div class="row no-gutters">
                                        <div class="col">
                                            <a class="dropdown-icon-item" href="#">
                                                <img src="{{asset('Backend/images/Servicemenu/conversation_sheet.png')}}" alt="Github">
                                                <span>Conversation Sheet</span>
                                            </a>
                                        </div>
                                        <div class="col">
                                            <a class="dropdown-icon-item" href="#">
                                                <img src="{{asset('Backend/images/Servicemenu/notes.png')}}" alt="dribbble">
                                                <span>Note</span>
                                            </a>
                                        </div>
                                        <div class="col">
                                            <a class="dropdown-icon-item" href="#">
                                                <img src="{{asset('Backend/images/Servicemenu/to-do-list.png')}}" alt="slack">
                                                <span>Task</span>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="row no-gutters">
                                        <div class="col">
                                            <a class="dropdown-icon-item" href="#">
                                                <img src="{{asset('Backend/images/Servicemenu/howtodo.png')}}" alt="G Suite">
                                                <span>How To Do</span>
                                            </a>
                                        </div>
                                        <div class="col">
                                            <a class="dropdown-icon-item" href="#">
                                                <img src="{{asset('Backend/images/Servicemenu/appointment.png')}}" alt="bitbucket">
                                                <span>Appointment</span>
                                            </a>
                                        </div>
                                        <div class="col">
                                            <a class="dropdown-icon-item" href="#">
                                                <img src="{{asset('Backend/images/Servicemenu/complain.png')}}" alt="dropbox">
                                                <span>Complaint</span>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="row no-gutters">
                                        <div class="col">
                                            <a class="dropdown-icon-item" href="#">
                                                <img src="{{asset('Backend/images/Servicemenu/proposal.png')}}" alt="G Suite">
                                                <span>Proposal</span>
                                            </a>
                                        </div>
                                        <div class="col">
                                            <a class="dropdown-icon-item" href="#">
                                                <img src="{{asset('Backend/images/Servicemenu/work.png')}}" alt="bitbucket">
                                                <span>My Work</span>
                                            </a>
                                        </div>
                                        <div class="col">
                                            <a class="dropdown-icon-item" href="#">
                                                <img src="{{asset('Backend/images/Servicemenu/envelope.png')}}" alt="dropbox">
                                                <span>Email Access</span>
                                            </a>
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </li>

                        <li class="dropdown notification-list topbar-dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <i class="fe-bell noti-icon"></i>
                                <span class="badge badge-danger rounded-circle noti-icon-badge">5</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-lg">

                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="m-0">
                                        <span class="float-right">
                                            <a href="#" class="text-dark">
                                                <small>Clear All</small>
                                            </a>
                                        </span>Notification
                                    </h5>
                                </div>

                                <div class="noti-scroll" data-simplebar>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                        <div class="notify-icon bg-soft-primary text-primary">
                                            <i class="mdi mdi-comment-account-outline"></i>
                                        </div>
                                        <p class="notify-details">Doug Dukes commented on Admin Dashboard
                                            <small class="text-muted">1 min ago</small>
                                        </p>
                                    </a>
                                </div>

                                <!-- All-->
                                <a href="javascript:void(0);" class="dropdown-item text-center text-primary notify-item notify-all">
                                    View all
                                    <i class="fe-arrow-right"></i>
                                </a>

                            </div>
                        </li>

                        <li class="dropdown notification-list topbar-dropdown">
                            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="{{asset('Backend/images/users/avatar-1.jpg')}}" alt="user-image" class="rounded-circle">
                                <span class="pro-user-name ml-1">
                                    Nik Patel <i class="mdi mdi-chevron-down"></i>
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <!-- item-->
                                <div class="dropdown-header noti-title">
                                    <h6 class="text-overflow m-0">Welcome !</h6>
                                </div>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="ri-account-circle-line"></i>
                                    <span>My Account</span>
                                </a>

                                <!-- item-->
                                <a href="{{route('admin.profile')}}" class="dropdown-item notify-item">
                                    <i class="ri-user-2-line"></i>
                                    <span>Profile</span>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="ri-lock-line"></i>
                                    <span>Lock Screen</span>
                                </a>

                                <div class="dropdown-divider"></div>

                                <!-- item-->

                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();" class="dropdown-item notify-item">
                                        <i class="ri-logout-box-line"></i>
                                        <span>Logout</span>
                                    </a>
                                </form>

                            </div>
                        </li>

                    </ul>

                    <!-- LOGO -->
                    <div class="logo-box">
                        <a href="" class="logo logo-dark text-center">
                            <span class="logo-sm">
                                <img src="{{asset('Backend/images/fsc_logo.png')}}" alt="" height="24">
                                <!-- <span class="logo-lg-text-light">Minton</span> -->
                            </span>

                        </a>

                        <a href="{{route('dashboard')}}" class="logo logo-light text-center">
                            <span class="logo-sm">
                                <img src="{{asset('Backend/images/favicon.png')}}" alt="" height="24">
                            </span>

                        </a>
                    </div>

                    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                        <li>
                            <button class="button-menu-mobile waves-effect waves-light">
                                <i class="fe-menu"></i>
                            </button>
                        </li>

                        <li>
                            <!-- Mobile menu toggle (Horizontal Layout)-->
                            <a class="navbar-toggle nav-link" data-toggle="collapse" data-target="#topnav-menu-content">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- end Topbar -->

            @yield('backend_content')

        </div> <!-- content -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="text-md-right footer-links d-none d-sm-block">
                            <a href="javascript:void(0);">About Us</a>
                            <a href="javascript:void(0);">Help</a>
                            <a href="javascript:void(0);">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

    </div>
    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->
@yield('customModels')

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

<!-- Vendor js -->
<script src="{{asset('Backend/js/vendor.min.js')}}"></script>

<!-- KNOB JS -->
<script src="{{asset('Backend/libs/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- Apex js-->
<script src="{{asset('Backend/libs/apexcharts/apexcharts.min.js')}}"></script>

<!-- Dashboard init-->
<script src="{{asset('Backend/js/pages/dashboard-sales.init.js')}}"></script>

<!-- App js -->
<script src="{{asset('Backend/js/app.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" type="text/javascript"></script>

@yield('scripts')

<script type="text/javascript">
    $('.input-tel-mask').mask('(999) 999-9999');
    $('.input-usa-zip-mask').mask('99999');
    $('.input-extension-mask').mask('9999');
    $('.input-fax-no-mask').mask('(999) 999-9999');
    $('.datepickers').datepicker({
        autoclose: true,
        orientation: 'bottom auto',
    });
    $('.js-select-2').select2();
</script>
</body>
</html>
