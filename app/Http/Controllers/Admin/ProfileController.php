<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Contact;
use App\Models\Admin\Formation;
use App\Models\Admin\Profile;
use App\Models\Admin\Security;
use App\Models\Admin\Shareholder;
use File;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $isProfileExsist = Profile::all()->count();
        $isContactExist = Contact::all()->count();
        $isSecurityExist = Security::all()->count();
        $isFormationExist = Formation::all()->count();
        //exit();
        $companyInfo = Profile::where('admin_id', Auth::user()->id)->first();
        $contactInfo = Contact::where('admin_id', Auth::user()->id)->first();
        $securityInfo = Security::where('admin_id', Auth::user()->id)->first();
        $formationInfo = Formation::where('admin_id', Auth::user()->id)->first();

        $shareHolders = Shareholder::where('formation_id', '1')->get();

        return view('Backend.Admin.profile', compact('shareHolders', 'isFormationExist', 'formationInfo', 'isSecurityExist', 'securityInfo', 'isProfileExsist', 'companyInfo', 'isContactExist', 'contactInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function removeCertificate(Request $request)
    {
        try {
            $id = $request->id;
            $certificate = $request->certificate;
            if ($certificate == 'certificate') {
                Formation::where('id', $id)->update([
                    'certificate' => ''
                ]);
            } else if ($certificate == 'article') {
                Formation::where('id', $id)->update([
                    'articles' => ''
                ]);
            }
            echo 'Success';
        } catch (\Exception $ex) {
            abort(500);
            echo 'Error';
        }

    }

    public function storeCompanyInfo(Request $request)
    {

        Validator::make($request->all(), [
            'company_name' => ['required'],
            'business_name' => ['required'],
            'address_1' => ['required'],
            'city' => ['required'],
            'state' => ['required'],
            'zip' => ['required'],
            'pa_city' => ['required'],
            'pa_state' => ['required'],
            'pa_zip' => ['required'],
            'contact_no' => ['required']
        ])->validate();

        if ($request->has('profile_id')) {
            $id = $request->profile_id;
            $input = $request->except(['_token', 'profile_id', 'pa_state__']);

            $input['state_code'] = 'None';
            $input['pa_state_code'] = 'None';
            Profile::where('id', $id)->update($input);
        } else {
            $input = $request->all();
            $input['state_code'] = 'GA';
            $input['pa_state_code'] = 'GA';
            //Profile::create($input);
        }
        return redirect(route('admin.profile'));
    }

    public function storeContact(Request $request)
    {
        Validator::make($request->all(), [
            'fname' => ['required'],
            'mname' => ['required'],
            'lname' => ['required'],
            'city' => ['required'],
            'state' => ['required'],
            'zip' => ['required'],
            'contact_no_1' => ['required'],
            'contact_no_type_1' => ['required'],
            'contact_no_extension_1' => ['required']
        ])->validate();

        if ($request->has('contact_id')) {
            $id = $request->contact_id;
            $input = $request->except(['_token', 'contact_id']);
            if ($request->has('same_as_company_fax_no')) {
                if ($request->same_as_company_fax_no == "on") {
                    $input['same_as_company_fax_no'] = 1;
                } else {
                    $input['same_as_company_fax_no'] = 0;
                }
            }
            //$input['state_code']='None';
            //$input['pa_state_code']='None';
            Contact::where('id', $id)->update($input);
        } else {
            $input = $request->all();
            if ($request->has('same_as_company_fax_no')) {
                if ($request->same_as_company_fax_no == "on") {
                    $input['same_as_company_fax_no'] = 1;
                } else {
                    $input['same_as_company_fax_no'] = 0;
                }
            }
            Contact::create($input);
        }
        return redirect(route('admin.profile'));

    }

    public function storeSecurity(Request $request)
    {
        if ($request->has('security_id')) {
            $id = $request->security_id;
            $input = $request->except(['_token', 'security_id']);
            Security::where('id', $id)->update($input);
        } else {
            $input = $request->all();
            Security::create($input);
        }
        return redirect(route('admin.profile'));
    }

    public function storeFormation(Request $request)
    {
        try {
            $upload_certificate = '';
            $upload_article = '';
            if ($request->hasFile('certificate')) {
                $fileName = str_replace(' ', '_', $_FILES['certificate']['name']);
                $upload_certificate = date('mdYHis') . uniqid() . $fileName;
                $request->certificate->move('company_document/certificate', $upload_certificate);
            }
            if ($request->hasFile('articles')) {
                $fileName = str_replace(' ', '_', $_FILES['articles']['name']);
                $upload_article = date('mdYHis') . uniqid() . $fileName;
                $request->articles->move('company_document/article', $upload_article);
            }

            if ($request->has('formation_id')) {
                $id = $request->formation_id;
                //$input=$request->all();
                $input = $request->except(['_token', 'formation_id']);
                $input['certificate'] = $upload_certificate;
                $input['articles'] = $upload_article;
                $input['date_of_incorporation'] = date('Y-m-d', strtotime($request->date_of_incorporation));
                $input['renewal_date'] = date('Y-m-d', strtotime($request->renewal_date));
                Formation::where('id', $id)->update($input);
            } else {
                $input = $request->all();
                $input['certificate'] = $upload_certificate;
                $input['articles'] = $upload_article;
                $input['date_of_incorporation'] = date('Y-m-d', strtotime($request->date_of_incorporation));
                $input['renewal_date'] = date('Y-m-d', strtotime($request->renewal_date));
                Formation::create($input);
            }
        } catch (Exception $ex) {
            unlink(public_path('company_document/certificate/' . $upload_certificate));
            unlink(public_path('company_document/article/' . $upload_article));
            dd($ex->getMessage());
        } catch (QueryException $ex) {
            unlink(public_path('company_document/certificate/' . $upload_certificate));
            unlink(public_path('company_document/article/' . $upload_article));
        } catch (\ErrorException $ex) {
            unlink(public_path('company_document/certificate/' . $upload_certificate));
            unlink(public_path('company_document/article/' . $upload_article));
        }

        return redirect(route('admin.profile'));
    }

    public function storeShareHolder(Request $request)
    {
        try {
            if ($request->hasFile('id')) {

            } else {
                $input = $request->except(['_token']);
                $input['effective_date'] = date('Y-m-d', strtotime($request->effective_date));
                $isStore = Shareholder::create($input);
                if ($isStore) {
                    $shareholder = Shareholder::Where('formation_id', '1')->get();
                    echo json_encode($shareholder);
                } else {
                    echo '2';
                }
            }
        } catch (QueryException $ex) {
            abort(405);
        } catch (Exception $ex) {
            abort(405);
        }
    }

}
