<?php


namespace App\Utility;


class DateUtility
{

    public function formats($date)
    {
        return date('Y-m-d',strtotime($date));
    }

    public function retriveDate($date)
    {
        return date('d/m/Y',strtotime($date));
    }
}
