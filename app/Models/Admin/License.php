<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class License extends Model
{
    protected $table="admin_license";
    use HasFactory;
    protected $fillable=[
      'admin_id',
      'year',
      'gross_revenue',
      'license_fee',
      'license_status',
      'license_copy',
      'license_issue_date',
      'license_website',
      'license_note',
    ];
}
