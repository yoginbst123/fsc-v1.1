<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShareLedger extends Model
{
    public $table = "admin_share_ledger";
    use HasFactory;

    protected $fillable = [
        'admin_id',
        'holder_name',
        'issue_certificate',
        'origine_issue_form',
        'paid_amount',
        'transfer_date',
        'surrender_certificate',
        'surrender_certificate_no'
    ];
}
