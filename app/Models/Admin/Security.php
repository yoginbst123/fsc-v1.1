<?php

namespace App\Models\Admin;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Security extends Model
{
    use HasFactory;

    protected $table = "admin_security_info";

    protected $fillable = [
        'admin_id',
        'question1',
        'que_1',
        'question2',
        'que_2',
        'question3',
        'que_3',
        'answer1',
        'answer2',
        'answer3',
    ];

    public function admins()
    {
        return $this->belongsTo(Admin::class);
    }
}
